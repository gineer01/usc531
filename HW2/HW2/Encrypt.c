//
//  Encrypt.c
//  HW2
//
//  Created by Cuong Dong on 2/14/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include "Stream.h"
#include "Pbm.h"
#include "Util.h"


void encryptBit(int inputBit, int keyBit, Pbm *out1, Pbm *out2, int width, int height);

//Given Pbm struct and a passphrase, encrypt and output to 2 Pbm struct
void encryptPbm(const char *passPhrase, Pbm *in, Pbm *out1, Pbm *out2) {
    StreamState state = createStreamState((const unsigned char*)passPhrase);
    unsigned char keyStream[OUTPUT_SIZE];
    int bitsPerGeneration = OUTPUT_SIZE * 8;
    int i = 0;
    for(int h = 0; h < in->height; h++) {
        for(int w = 0; w < in->width; w++){
            if (i % bitsPerGeneration == 0){
                generateStream(&state, keyStream);
                i = 0;
            }
            int byteLocation = i / 8;
            int bitLocation = i % 8;
            int keyBit = getBitInStream(keyStream, byteLocation, bitLocation);
            int inputBit = getBit(in, w, h);
            encryptBit(inputBit, keyBit, out1, out2, w, h);
            i++;
        }
    }
    deleteStreamState(&state);
}

//Encrypt the specified input file using the provided passphrase and output to 2 files
//Handle conversion from FILE to Pbm and write Pbm to FILE
void encrypt(const char *passPhrase, FILE *input, FILE *output1, FILE *output2){    
    Pbm in = createPbm(input);
    Pbm out1 = createEmptyPbm(in.width * 2, in.height * 2);
    Pbm out2 = createEmptyPbm(in.width * 2, in.height * 2);
    
    encryptPbm(passPhrase, &in, &out1, &out2);
    
    writeToFile(&out1, output1);
    writeToFile(&out2, output2);
    
    deletePbm(&in);
    deletePbm(&out1);
    deletePbm(&out2);
}

//Merge the two input files and output to the output file
//Handle conversion from FILE to Pbm and write Pbm to FILE
void merge(FILE *input1, FILE *input2, FILE *output){
    Pbm in1 = createPbm(input1);
    Pbm in2 = createPbm(input2);
    if ((in1.width != in2.width) || (in1.height != in2.height)){
        printError("Two input files must have the same size (width and height)");
    }
    Pbm out = createEmptyPbm(in1.width, in1.height);
    
    mergePbm(&in1, &in2, &out);
    writeToFile(&out, output);
    
    deletePbm(&out);
    deletePbm(&in1);
    deletePbm(&in2);
}

//Check a pattern of 4 pixels to decrypt them back to the original pixel
// Returns 0 if the original pixel is white and 1 if black, exit if invalid
int checkPattern(int h, int w, Pbm *in) {
    int a1 = getBit(in, w, h);
    int a2 = getBit(in, w + 1, h);
    int b1 = getBit(in, w, h + 1);
    int b2 = getBit(in, w + 1, h + 1);
    
    if ((a1 == 1) && (a2 == 1) &&
        (b1 == 1) && (b2 == 1)){
        return 1;
    }
    else if ((a1 == 0) && (a2 == 1) &&
             (b1 == 1) && (b2 == 0)){
        return 0;
    }
    else if ((a1 == 1) && (a2 == 0) &&
             (b1 == 0) && (b2 == 1)){
        return 0;
    }
    else {
        printError("Invalid pattern found in merged file");
        return -1;
    }   
}

//Decrypt the provided Pbm and output to out Pbm
void decryptPbm(Pbm *in, Pbm *out){
    for(int h = 0; h < in->height; h += 2) {
        for(int w = 0; w < in->width; w += 2){
            int pattern = checkPattern(h, w, in);
            setBit(out, w/2, h/2, pattern == 1);
        }
    }
}

//Decrypt the input file and output the result to the output file
//Handle conversion from FILE to Pbm and write Pbm to FILE
void decrypt(FILE *input, FILE *output){
    Pbm in = createPbm(input);    
    Pbm out = createEmptyPbm(in.width/2, in.height/2);
    
    decryptPbm(&in, &out);
    writeToFile(&out, output);
    
    deletePbm(&out);
    deletePbm(&in);
}

//Set bit according to pattern 1: [[1 0], [0 1]]
void pattern1(int height, int width, Pbm *out) {
    setBit(out, width * 2, height * 2, true);
    setBit(out, width * 2 + 1, height * 2, false);
    setBit(out, width * 2, height * 2 + 1, false);
    setBit(out, width * 2 + 1, height * 2 + 1, true);
}

//Set bit according to pattern 2: [[0 1], [1 0]]
void pattern2(int height, int width, Pbm *out) {
    setBit(out, width * 2, height * 2, false);
    setBit(out, width * 2 + 1, height * 2, true);
    setBit(out, width * 2, height * 2 + 1, true);
    setBit(out, width * 2 + 1, height * 2 + 1, false);
}

//Generate the pattern for the provided input bit and key bit for 2 output PBM at the specified
//location (w, h)
void encryptBit(int inputBit, int keyBit, Pbm *out1, Pbm *out2, int width, int height){
    if (inputBit == 0){
        if (keyBit == 0){
            pattern1(height, width, out1);
            pattern1(height, width, out2);
        }
        else {
            pattern2(height, width, out1);
            pattern2(height, width, out2);
        }
    }
    else {
        if (keyBit == 0){
            pattern1(height, width, out1);
            pattern2(height, width, out2);
        }
        else {
            pattern2(height, width, out1);
            pattern1(height, width, out2);
        }
    }
}

