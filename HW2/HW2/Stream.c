//
//  Stream.c
//  HW2
//
//  Created by Cuong Dong on 2/12/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <openssl/md5.h>
#include "Stream.h"
#include "Util.h"

//Generate a key stream to output for each function call
//state is a pointer to a StreamState created by createStreamState() function below
//output is buffer of OUTPUT_SIZE bytes
void generateStream(StreamState *state, unsigned char *output){
    /*
     * Begin code I did not write (with some change)
     * The following code was obtained from CSCI 531 HW2 http://merlot.usc.edu/cs531-s13/homeworks/hw2/
     */
    unsigned char md5_buf[MD5_DIGEST_LENGTH];
    unsigned char *s = state->buffer;
    int len = state->length;
    
    MD5(s, len, md5_buf);
    memcpy(output, md5_buf, MD5_DIGEST_LENGTH>>1);
    
    //Update state for next generation
    if (++(state->i) == 100) {
        state->i = 0;
    }
    sprintf((char *)s + MD5_DIGEST_LENGTH, "%02d%s", state->i, state->passPhrase);
    memcpy(s, md5_buf, MD5_DIGEST_LENGTH);
    //End of Code I did not write from CSCI 531 HW2
}

//Create and initialize the buffer and counter
//Buffer needs to be initialized with MD5 of the passphrase + counter + passphrase
StreamState createStreamState(const unsigned char *passPhrase){
    int passLength = (int) strlen((char*) passPhrase);
    int len = passLength + 2 + MD5_DIGEST_LENGTH;
    unsigned char *s = malloc(len+1);
    
    if (s == NULL){
        printError("Error allocating memory");
    }
    
    /*
     * Begin code I did not write (with some change)
     * The following code was obtained from CSCI 531 HW2 http://merlot.usc.edu/cs531-s13/homeworks/hw2/
     */
    unsigned char md5_buf[MD5_DIGEST_LENGTH];
    MD5(passPhrase, passLength, md5_buf);
    int i = 0;
    sprintf((char*)(s + MD5_DIGEST_LENGTH), "%02d%s", i, passPhrase);
    memcpy(s, md5_buf, MD5_DIGEST_LENGTH);
    //End of Code I did not write from CSCI 531 HW2
    
    StreamState state = {s, len, passPhrase, i};
    return state;
}

//Delete the allocated buffer in StreamState
void deleteStreamState(StreamState *s){
    free(s->buffer);
}
