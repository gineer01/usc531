//
//  Util.c
//  HW2
//
//  Created by Cuong Dong on 2/14/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

//Print the provided message to stderr and exit
void printError(const char *message) {
    fprintf(stderr, "%s\n", message);
    exit(1);
}

//Open a file with provided name as binary for writing
FILE* openOutputFile(const char * name){
    FILE *input;
    input = fopen((char*)name, "wb");
    //check error and exit early
    if (input == NULL){
        perror("Cannot open the specified file");
        exit(1);
    }
    return input;
}

//Open a file with provided name as binary for reading
FILE * openFile(const char *fileName) {
    FILE *input;
    input = fopen(fileName, "rb");
    //check error and exit early
    if (input == NULL){
        perror("Cannot open the specified file");
        exit(1);
    }
    return input;
}

//Get a specific bit within a specific byte 
int getBitInStream(unsigned char *stream, int byteLocation, int bitLocation){
    const unsigned char mask = 1 << (7 - bitLocation);
    if ((stream[byteLocation] & mask) == 0){
        return 0;
    }
    else return 1;
}


