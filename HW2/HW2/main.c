//
//  main.c
//  HW2
//
//  Created by Cuong Dong on 2/12/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include "CommandParser.h"
#include "Stream.h"
#include "Encrypt.h"
#include "Util.h"

//Functions to execute the selected command with the provided options
int doStream(CommandOptions options);
int doEncrypt(CommandOptions options);
int doDecrypt(CommandOptions options);
int doMerge(CommandOptions options);

//Parse the command line arguments and dispatch to the corresponding function
int main(int argc, char * argv[]){
    CommandOptions options = parseCommandLine(argc, argv);
    switch(options.command){
        case STREAM: return doStream(options);
        case ENCRYPT: return doEncrypt(options);
        case MERGE: return doMerge(options);
        case DECRYPT: return doDecrypt(options);
        default:
            return 0;
    }
}

//Do stream: call the generateStream() function repeatedly and write to stdout
int doStream(CommandOptions options){
    StreamState state = createStreamState((unsigned char *) options.passPhrase);
    unsigned char output[OUTPUT_SIZE];
    int i = options.l;
    int success = 0;
    while (i > 0){
        generateStream(&state, output);
        int n = i > OUTPUT_SIZE ? OUTPUT_SIZE : i;
        size_t result = fwrite(output, sizeof(char), n, stdout);
        if (result < n){
            fprintf(stderr, "Error while writing to output for stream command");
            success = 1;
            break;
        }
        i -= result;
    }
    deleteStreamState(&state);
    return success;
}

//Encrypt command: handle file opening/closing and delegate to encrypt() 
int doEncrypt(CommandOptions options){
    FILE *input = options.isStdin ? stdin : openFile(options.pbmFile);
    
    unsigned long l = strlen(options.outName) + strlen(".1.pbm") + 1;
    char name[l];
    sprintf(name, "%s.1.pbm", options.outName);
    FILE *output1 = openOutputFile(name);
    sprintf(name, "%s.2.pbm", options.outName);
    FILE *output2 = openOutputFile(name);
    
    encrypt((char *) options.passPhrase, input, output1, output2);
    
    fclose(output2);
    fclose(output1);
    if (!options.isStdin){
        fclose(input);
    }
    
    return 0;
}

//Merge command: handle file opening/closing and delegate to merge()
int doMerge(CommandOptions options){
    FILE *output = stdout;
    FILE *input1 = openFile(options.pbmFile1);
    FILE *input2 = openFile(options.pbmFile2);
    
    merge(input1, input2, output);
    
    fclose(input1);
    fclose(input2);
    
    return 0;
}

//Decrypt command: handle file opening/closing and delegate to decrypt()
int doDecrypt(CommandOptions options){
    FILE *input = options.isStdin ? stdin : openFile(options.pbmFile);
    FILE *output = stdout;
    
    decrypt(input, output);
    
    if (!options.isStdin){
        fclose(input);
    }
    return 0;
}
