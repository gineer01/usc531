//
//  CommandParser.c
//  HW2
//
//  Created by Cuong Dong on 2/12/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include "CommandParser.h"

#define PASS_PHRASE "p"
#define OUTPUT_NAME "out"
#define LENGTH "l"

//Functions to parse command options after the command is known
CommandOptions parseDecrypt(int argc, char *argv[]);
CommandOptions parseStream(int argc, char *argv[]);
CommandOptions parseEncrypt(int argc, char *argv[]);
CommandOptions parseMerge(int argc, char *argv[]);

//A pair of key and value (e.g., p="password")
typedef struct {
    const char *key;
    const char *value;
} Pair;

//Print usage to user when an invalid command option is found and exit
void printUsage(){
    char * usage = "\nThe commandline syntax for hw2 is as follows:\n"    
    "hw2 stream -p=pphrase -l=len\n"
    "hw2 encrypt -p=pphrase -out=name [pbmfile]\n"
    "hw2 merge pbmfile1 pbmfile2\n"
    "hw2 decrypt [pbmfile]\n";
    fprintf(stderr, "%s", usage);
    exit(1);
}

//Print an error message and then print the proper usage and exit
void printErrorAndUsage(const char *message) {
    fprintf(stderr, "%s", message);
    printUsage();
}

//Parse a string of format <key>=<value> to a Pair
Pair getPair(char *argv){
    Pair pair = {"", ""};
    char *key = argv;
    for(char *value = argv; *value != 0; value++){
        if (*value == '='){
            *value = 0;
            value++;
            pair.key = key;
            pair.value = value;
            return pair;
        }
    }
    
    printErrorAndUsage("Wrong command option (missing =)");
    
    return pair;
}

//Parse the command and dispatch to the corresponding function
CommandOptions parseCommandLine(int argc, char *argv[]){    
    if (argc <= 1) {
        printErrorAndUsage("Please provide a command\n");
    }
    
    if (strcmp(argv[1], "stream") == 0) {
        return parseStream(argc, argv);
    } else if (strcmp(argv[1], "encrypt") == 0) {
        return parseEncrypt(argc, argv);
    } else if (strcmp(argv[1], "merge") == 0) {
        return parseMerge(argc, argv);
    } else if (strcmp(argv[1], "decrypt") == 0) {
        return parseDecrypt(argc, argv);
    } else {
        printErrorAndUsage("Invalid command \n");
    }
    
    exit(1);
}

//Parse the command options for decrypt command
CommandOptions parseDecrypt(int argc, char *argv[]){
    CommandOptions options = {DECRYPT, "", 0, "", "", false, "", ""};
    if (argc == 2){
        options.isStdin = true;
    }
    else if (argc == 3){
        options.isStdin = false;
        options.pbmFile = argv[2];
    }
    else {
        printErrorAndUsage("Invalid number of arguments \n");
    }
    return options;
}

//Parse the command options for merge command
CommandOptions parseMerge(int argc, char *argv[]){
    CommandOptions options = {MERGE, "", 0, "", "", false, "", ""};
    if (argc == 4){
        options.pbmFile1 = argv[2];
        options.pbmFile2 = argv[3];
    }
    else {
        printErrorAndUsage("Invalid number of arguments \n");
    }
    return options;
}

//Parse the command options for encrypt command
CommandOptions parseEncrypt(int argc, char *argv[]){
    CommandOptions options = {ENCRYPT, "", 0, "", "", false, "", ""};
    if (argc == 4){
        options.isStdin = true;
    }
    else if (argc == 5){
        options.isStdin = false;
        options.pbmFile = argv[4];
    }
    else {
        printErrorAndUsage("Invalid number of arguments.\n");
    }
    
    for (int i = 2; i < 4; i++) {
        if (argv[i][0] == '-'){
            Pair pair = getPair(argv[i] + 1);
            if (strcmp(pair.key, PASS_PHRASE) == 0){
                options.passPhrase = pair.value;
            }
            else if (strcmp(pair.key, OUTPUT_NAME) == 0){
                options.outName = pair.value;
            }
            else {
                printErrorAndUsage("Invalid command option.\n");
            }
        }
        else {
            printErrorAndUsage("Invalid command option (missing -) \n");
        }
    }
    
    return options;
}

//Parse the command options for stream command
CommandOptions parseStream(int argc, char *argv[]){
    CommandOptions options = {STREAM, "", 0, "", "", false, "", ""};
    if (argc == 4){
        for (int i = 2; i < argc; i++) {
            if (argv[i][0] == '-'){
                Pair pair = getPair(argv[i] + 1);
                if (strcmp(pair.key, PASS_PHRASE) == 0){
                    options.passPhrase = pair.value;
                }
                else if (strcmp(pair.key, LENGTH) == 0){
                    options.l = atoi(pair.value);
                    if (options.l < 0){
                        printErrorAndUsage("Length cannot be negative.\n");
                    }
                }
                else {
                    printErrorAndUsage("Invalid command option.\n");
                }
            }
            else {
                printErrorAndUsage("Invalid command option (missing -) \n");
            }
        }
    }
    else {
        printErrorAndUsage("Invalid number of arguments \n");
    }
    
    return options;
}

