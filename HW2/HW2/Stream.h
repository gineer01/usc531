//
//  Stream.h
//  HW2
//
//  Created by Cuong Dong on 2/12/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW2_Stream_h
#define HW2_Stream_h

//struct to describe the current stream state (the current buffer and current counter)
// as well as the passphrase used to generate
typedef struct {
    unsigned char *buffer;
    int length;
    const unsigned char *passPhrase;
    int i;
} StreamState;

//The size of the generated stream for each iteration
#define OUTPUT_SIZE 8

//Generate a key stream to output for each function call
//state is a pointer to a StreamState created by createStreamState() function below
//output is buffer of OUTPUT_SIZE bytes
void generateStream(StreamState *state, unsigned char *output);

//Create and initialize the buffer and counter
StreamState createStreamState(const unsigned char *passPhrase);

//Delete the allocated buffer in StreamState
void deleteStreamState(StreamState *s);

#endif
