//
//  Pbm.h
//  HW2
//
//  Created by Cuong Dong on 2/14/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW2_Pbm_h
#define HW2_Pbm_h

#include <stdbool.h>

//struct to describe a PBM data
typedef struct {
    int width;
    int height;
    unsigned char *content;
} Pbm;

//Read from file and create a PBM
Pbm createPbm(FILE *file);

//Create an empty PBM with the specified sizes
Pbm createEmptyPbm(int width, int height);

//Get bit at location (w,h) in PBM
int getBit(Pbm *pbm, int w, int h);

//Set bit at the specified location (w,h)
void setBit(Pbm *pbm, int w, int h, bool set);

//Visualize a PBM file in stdout ('#' for each set bit)
void visualize(Pbm *pbm);

//Write PBM data to a file according to PBM file format
void writeToFile(Pbm *pbm, FILE *file);

//Merge data in 2 PBM's using OR and write to output PBM
void mergePbm(Pbm *in1, Pbm *in2, Pbm *out);

//Destruct a PBM (deallocate memory)
void deletePbm(const Pbm *pbm);
#endif
