//
//  Util.h
//  HW2
//
//  Created by Cuong Dong on 2/14/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW2_Util_h
#define HW2_Util_h

//Print the provided message to stderr and exit
void printError(const char *message);

//Open a file with provided name as binary for writing
FILE* openOutputFile(const char * name);

//Open a file with provided name as binary for reading
FILE * openFile(const char *fileName);

//Get a specific bit within a specific byte 
int getBitInStream(unsigned char *stream, int byteLocation, int bitLocation);
#endif
