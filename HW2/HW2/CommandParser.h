//
//  CommandParser.h
//  HW2
//
//  Created by Cuong Dong on 2/12/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW2_CommandParser_h
#define HW2_CommandParser_h

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

//Enumeration of all possible commands
typedef enum {STREAM, ENCRYPT, MERGE, DECRYPT} COMMAND;

//struct to capture all possible command options
typedef struct {
    COMMAND command;
    const char *passPhrase;
    int l;
    const char *outName;
    const char *pbmFile;
    bool isStdin;
    const char *pbmFile1;
    const char *pbmFile2;
} CommandOptions;

//Parse the command line argument and return CommandOptions struct
CommandOptions parseCommandLine(int argc, char *argv[]);

#endif
