//
//  Encrypt.h
//  HW2
//
//  Created by Cuong Dong on 2/14/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW2_Encrypt_h
#define HW2_Encrypt_h

//Encrypt the specified input file using the provided passphrase and output to 2 files
void encrypt(const char *passPhrase, FILE *input, FILE *output1, FILE *output2);

//Merge the two input files and output to the output file
void merge(FILE *input1, FILE *input2, FILE *output);

//Decrypt the input file and output the result to the output file
void decrypt(FILE *input, FILE *output);
#endif
