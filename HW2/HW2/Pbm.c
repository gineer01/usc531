//
//  Pbm.c
//  HW2
//
//  Created by Cuong Dong on 2/14/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Pbm.h"
#include "Util.h"

//Get the length of each line in byte
int getLineSize(int width) {
    int lineSize = width % 8 == 0 ? width/8 : width/8 + 1;
    return lineSize;
}

//Get the total size of PBM content in bytes
int getSize(Pbm *pbm){
    int lineSize = getLineSize(pbm->width);
    return pbm->height * lineSize;
}

//Read from file and create a PBM
Pbm createPbm(FILE *file){
    Pbm result;
    
    char magicString[3];
    
    fscanf(file, "%2s %d %d ", magicString, &result.width, &result.height);
    
    if (strcmp("P4", magicString) != 0){
        printError("The file is not PBM format (magic number is not P4)");
    }
    
    int lineSize = getLineSize(result.width);
    int size = result.height * lineSize;
    result.content = malloc(size);
    if (result.content == NULL){
        printError("Error allocating memory");
    }
    
    size_t readBytes = fread(result.content, sizeof(char), size, file);
    if (readBytes < size){
        printError("Error reading file");
    }
    
    return result;
}

//Create an empty PBM with the specified sizes
Pbm createEmptyPbm(int width, int height){
    Pbm result = {.width = width, .height = height};
    
    int lineSize = getLineSize(result.width);
    int size = result.height * lineSize;
    result.content = malloc(size);
    if (result.content == NULL){
        printError("Error allocating memory");
    }
    
    //Setting the last byte to 0 so that the padding bits are always 0
    for(int i = 0; i < height; i++){
        result.content[lineSize * (i + 1) -1] = 0;
    }
    
    return result;
}

//Get bit at location (w,h) in PBM
int getBit(Pbm *pbm, int w, int h){
    int byteLocation = h * getLineSize(pbm->width) + w/8;
    int bitLocation = w % 8;
    return getBitInStream(pbm->content, byteLocation, bitLocation);
}

//Set bit at the specified location (w,h)
void setBit(Pbm *pbm, int w, int h, bool set){
    int byteLocation = h * getLineSize(pbm->width) + w/8;
    int bitLocation = w % 8;
    const unsigned char mask = 1 << (7 - bitLocation);
    if (set){
        pbm->content[byteLocation] = pbm->content[byteLocation] | mask;
    }
    else {
        pbm->content[byteLocation] = pbm->content[byteLocation] & ~mask;
    }
}

//Visualize a PBM file in stdout ('#' for each set bit)
void visualize(Pbm *pbm){
    for(int h = 0; h < pbm->height; h++) {
        for(int w = 0; w < pbm->width; w++){
            if (getBit(pbm,  w, h)) {
                printf("#");
            }
            else {
                printf(" ");
            }
        }
        printf("\n");
    }
}

//Write PBM data to a file according to PBM file format
void writeToFile(Pbm *pbm, FILE *file){
    fprintf(file, "P4\n%d %d\n", pbm->width, pbm->height);
    fwrite(pbm->content, sizeof(char), getSize(pbm), file);
}

//Destruct a PBM (deallocate memory)
void deletePbm(const Pbm *pbm){
    free(pbm->content);
}

//Merge data in 2 PBM's using OR and write to output PBM
void mergePbm(Pbm *in1, Pbm *in2, Pbm *out){
    int lineSize = getLineSize(in1->width);
    for(int h = 0; h < in1->height; h++) {
        for(int b = 0; b < lineSize; b++){
            int byteLocation = h*lineSize + b;
            out->content[byteLocation] = in1->content[byteLocation] | in2->content[byteLocation];
        }
    }
}
