//
//  main.c
//  HW1
//
//  Created by Cuong Dong on 1/30/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "HexDump.h"
#include "Base64.h"


int main(int argc, char* arg[]){
    FILE *input;
    bool isFile = false;    

    if (argc == 1){
        fprintf(stderr, "Please provide a command to run. The valid commands are hexdump, enc-base64, and dec-base64\n");
        return 1;
    }
    
    char *command = arg[1];
    int c;//an enum is better
    
    if (strcmp("hexdump", command) == 0){
        c = 1;
    }
    else if (strcmp("enc-base64", command) == 0){
        c = 2;
    }
    else if (strcmp("dec-base64", command) == 0){
        c = 3;
    }
    else {
        fprintf(stderr, "Invalid command: %s. The valid commands are hexdump, enc-base64, and dec-base64\n", command);
        return 1;
    }
    
    if (argc > 2){
        //the argument after command is file name to be dumped
        input = fopen(arg[2], "rb");
        //check error and exit early
        if (input == NULL){
            perror("Cannot open the specified file");
            return 1;
        }
        isFile = true;
    }
    else if (argc == 2){
        input = stdin;
    }
    else return 1;
    
    switch(c){
        case 1: hexdump(input); break;
        case 2: encodeBase64(input); break;
        case 3: decodeBase64(input); break;
    }
    
    //Close file input but not stdin
    if (isFile){
        if (input != NULL){
            fclose(input);
        }
    }
    return 0;
}
