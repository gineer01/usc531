//
//  Base64.c
//  HW1
//
//  Created by Cuong Dong on 1/31/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>
#include "Base64.h"
#define INVALID 65

//Support function: decode a character in base64 to number
unsigned char decoding(unsigned char c){
    if ((c >= 'A') && (c <= 'Z')){
        return c - 'A';
    }
    else if ((c >= 'a') && (c <= 'z')){
        return c - 'a' + 26;
    }
    else if ((c >= '0') && (c <= '9')){
        return c - '0' + 52;
    }
    else if (c == '+'){
        return 62;
    }
    else if (c == '/'){
        return 63;
    }
    else if (c == '='){
        return 0;
    }
    else return INVALID;
}

//Support function: decode a buffer of 64 (or a multiple of 4) characters and output
bool decodeBuffer(unsigned char buffer[], int nBytes){
    if (nBytes % 4 != 0) {
        fprintf(stderr, "Invalid input: the number of bytes from input is not a multiple of 4\n");
        return false;
    }
    
    unsigned char out[48];
    int pos = 0;
    
    for (int i = 0; i + 3 < nBytes; i += 4){
        unsigned char c[4];
        for (int j = 0; j < 4; j++){
            unsigned char l = buffer[i + j];
            c[j] = decoding(l);
            if (c[j] == INVALID){
                fprintf(stderr, "Invalid input: invalid character %c is found \n", l);
                return false;
            }
        }
                    
        unsigned char oct1 = (c[0] << 2) | (c[1] >> 4);
        unsigned char oct2 = (c[1] << 4) | (c[2] >> 2);
        unsigned char oct3 = (c[2] << 6) | c[3];
        
        if (buffer[i + 2] == '='){
            out[pos++] = oct1;
        }
        else if (buffer[i + 3] == '='){
            out[pos++] = oct1;
            out[pos++] = oct2;            
        }
        else {
            out[pos++] = oct1;
            out[pos++] = oct2;
            out[pos++] = oct3;
        }
    }
    fwrite(out, 1, pos, stdout);
    return true;
}

int removeNewline(unsigned char buffer[], unsigned char fileBuffer[], int numberReadBytes, FILE *fp){
    int count = 0;
    for (int i = 0; i < numberReadBytes; i++){
        if (fileBuffer[i] != '\n'){
            buffer[count++] = fileBuffer[i];
        }
    }
    
    while ((count % 4) != 0){
        char temp[1];
        if (fread(temp, sizeof(char), 1, fp) == 0){
            return -1;
        }
        if (temp[0] != '\n'){
            buffer[count++] = temp[0];
        }
    }
    return count;
}

//Take a file pointer and decode the base64 content
int decodeBase64(FILE* fp){
    const int BUFFER_SIZE = 64;
    unsigned char fileBuffer[BUFFER_SIZE];
    unsigned char buffer[BUFFER_SIZE];
    bool readFlag = true;//flag whether to continue to read another block
    bool isValid = true;
    int numberReadBytes;//save the number of bytes actually read when buffering
    if (fp == NULL) {
        fprintf(stderr, "Null file pointer\n");
        return FILE_ERROR;
    }
    else {
        while (readFlag){
            if (!isValid){
                return INVALID_INPUT;
            }
            
            numberReadBytes = (int)fread(fileBuffer, sizeof(char), BUFFER_SIZE, fp);
            if (numberReadBytes < BUFFER_SIZE){
                if (ferror(fp)){
                    perror("Error while reading file");
                    return FILE_ERROR;
                }
                
                if (feof(fp)){//reaching the last block
                    readFlag = false;
                }
            }
            
            int nByte = removeNewline(buffer, fileBuffer, numberReadBytes, fp);
            if (nByte < 0){
                fprintf(stderr, "Error reading input\n");
                return FILE_ERROR;
            }
                   
            isValid = decodeBuffer(buffer, nByte);

        }
    }
    
    return SUCCESS;
}

//Support function: encode a buffer of 48 (or less) characters and output
// the base64 encoding in a 64-character (or less) line
void encodeBuffer(unsigned char buffer[], int nBytes){
    char *encoding = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    for (int i = 0; i < nBytes; i += 3){
        unsigned char oct1 = buffer[i];        
        bool pad1 = i + 1 >= nBytes;
        unsigned char oct2 = pad1 ? 0 : (buffer[i+1]);
        bool pad2 = i + 2 >= nBytes;
        unsigned char oct3 = pad2 ? 0 : (buffer[i+2]);        
        
        unsigned char c1 = encoding[(oct1 & 0xFC) >> 2];
        unsigned char c2 = encoding[((oct1 & 0x03) << 4) | ((oct2 & 0xF0) >> 4)];
        unsigned char c3 = pad1 ? '=' : encoding[((oct2 & 0x0F) << 2) | ((oct3 & 0xC0) >> 6)];
        unsigned char c4 = pad2 ? '=' : encoding[(oct3 & 0x3F)];
        printf("%c%c%c%c", c1, c2, c3, c4);
    }
    printf("\n");
}

//Take a file pointer and dump the content in base64 encoding
int encodeBase64(FILE* fp){
    const int BUFFER_SIZE = 48;
    unsigned char buffer[BUFFER_SIZE];
    bool readFlag = true;//flag whether to continue to read another block
    int numberReadBytes;//save the number of bytes actually read when buffering
    
    if (fp == NULL) {
        fprintf(stderr, "Null file pointer\n");
        return FILE_ERROR;
    }
    else {
        while (readFlag){
            numberReadBytes = (int)fread(buffer, sizeof(char), BUFFER_SIZE, fp);
            if (numberReadBytes < BUFFER_SIZE)//reaching the last block
                readFlag = false;
            encodeBuffer(buffer, numberReadBytes);
        }
    }
    
    return SUCCESS;
}
