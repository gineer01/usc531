//
//  HexDump.h
//  HW1
//
//  Created by Cuong Dong on 1/30/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

/**************************************************************
 * Citation: some of this hex dump code was done by myself for CMPE 130 class
 at San Jose State University (with some changes to meet new spec)
 
 * Author: Cuong Dong
 * Date: Oct 3, 2006
 * Project ID: Project 1
 * Class: CMPE 130
 *************************************************************/

#ifndef HW1_HexDump_h
#define HW1_HexDump_h
#include <stdio.h>

//The exit codes for hexdump
#define SUCCESS 0
#define NO_FILE_ERROR 2

//Constants
#define BYTE_PER_LINE 16

//Hexdump function: take a file pointer and dump the stream content in hexdump format
int hexdump(FILE* fp);

#endif
