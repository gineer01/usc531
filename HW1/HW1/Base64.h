//
//  Base64.h
//  HW1
//
//  Created by Cuong Dong on 1/31/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW1_Base64_h
#define HW1_Base64_h
#include <stdio.h>

#define SUCCESS 0
#define INVALID_INPUT 1
#define FILE_ERROR 2

//Take a file pointer and decode the base64 content
//It stops as soon as it sees an invalid 64-character line. A line must be 64
//(or a multiple of 4) characters and end with \n character.
int decodeBase64(FILE* fp);

//Take a file pointer and dump the content in base64 encoding
int encodeBase64(FILE* fp);

#endif
