//
//  HexDump.c
//  HW1
//
//  Created by Cuong Dong on 1/30/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

/**************************************************************
 * Citation: some of this hex dump code was done by myself for CMPE 130 class 
 at San Jose State University (with some changes to meet new spec)
 
 
 * Author: Cuong Dong
 * Date: Oct 3, 2006
 * Project: Project 1
 * Class: CMPE 130
 *************************************************************/

#include <stdbool.h>
#include "HexDump.h"

void printAsciiColumn(int startPosition, unsigned char *buffer, int nBytes) {
    //Print ASCII column
    unsigned char c;
    for (int j = 0; j < nBytes; j++){
        c = buffer[startPosition + j];
        if ((c >= 0x20) && (c <= 0x7e)){
            printf("%c", c);
        }
        else if ((c <= 0x1f) || (c == 0x7f)){
            printf(".");
        }
        else {
            printf("~");
        }
    }
    //print white spaces if the last line has less than BYTE_PER_LINE character
    for (int j = nBytes; j < BYTE_PER_LINE; j++){
        printf(" ");
    }
}

void printHexadecimal(int position, unsigned char *buffer, int nBytes) {
    //print the hexadecimals
    unsigned char c;
    for (int j = 0; j < nBytes; j++){
        if (j == 8) printf(" ");
        c = buffer[position + j];
        printf("%02x ", c);
    }
    //print white spaces if the last line has less than BYTE_PER_LINE character
    for (int j = nBytes; j < BYTE_PER_LINE; j++){
        if (j == 8) printf(" ");
        printf("-- ");
    }
}

//DisplayBuffer function will display the buffer in the required format
//Precondition: buffer is a array of char to be displayed, nByte is the number of
//      bytes in buffer to be displayed, pos is the address of the block in file to
//      correctly display the address before each line.
void displayBuffer(unsigned char *buffer, int nBytes, int pos){
    int position = 0;   //current position in the block (pos is the current position in the file)
    int byteInLine;     //the number of bytes in each line
    int nLine = ((nBytes % BYTE_PER_LINE) == 0) ? nBytes / BYTE_PER_LINE : (nBytes / BYTE_PER_LINE) + 1;
    for(int i = 0; i < nLine; i++){//for each line do
        //print the address (current position in file) before each line
        printf("%06x: ", pos);
        
        //determine the number of bytes in line
        byteInLine = ((nBytes - position) < BYTE_PER_LINE) ? nBytes - position : BYTE_PER_LINE;
        
        printHexadecimal(position, buffer, byteInLine);
        
        printf(" ");
        
        printAsciiColumn(position, buffer, byteInLine);
        
        printf("\n");
        
        //Update the line position and buffer position
        pos += byteInLine;
        position += byteInLine;
    }
}

//The hexdump fuction will read from file to a buffer, and
//call display buffer to display the buffer in the required format.
int hexdump(FILE* fp){
    const int BUFFER_SIZE = 256;
    unsigned char buffer[BUFFER_SIZE];
    bool readFlag = true;//flag whether to continue to read another block
    int numberReadBytes;//save the number of bytes actually read when buffering
    int pos = 0;//the position of block
    
    if (fp == NULL) {
        fprintf(stderr, "Null file pointer\n");
        return NO_FILE_ERROR;
    }
    else {
        while (readFlag){
            numberReadBytes = (int)fread(buffer, sizeof(char), BUFFER_SIZE, fp);
            if (numberReadBytes < BUFFER_SIZE)//reaching the last block
                readFlag = false;
            displayBuffer(buffer, numberReadBytes, pos);
            pos += numberReadBytes; //update block position
        }
    }
    return SUCCESS;
}
