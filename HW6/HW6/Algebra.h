//
//  Algebra.h
//  HW6
//
//  Created by Cuong Dong on 4/14/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW6_Algebra_h
#define HW6_Algebra_h

#include <stdbool.h>

#define POLY_SIZE 4

typedef unsigned char GF_2_8[POLY_SIZE];
typedef unsigned char Z_2;

//Convert hex string to polynomials with coefficients in GF(2^8)
void hexToPoly(GF_2_8 poly, const char *hex);

//Print polynomials in {00}{00}{00}{00} format
void printPoly(GF_2_8 poly);

//Product modulo x^4 + 1
void modProduct(GF_2_8 a, GF_2_8 b, GF_2_8 result);

//Find inverse using Euclidean algorithm
bool modInverse(GF_2_8 a, GF_2_8 inverse);

//Find inverse using closed-form formula
bool modInverseByMatrix(GF_2_8 in, GF_2_8 result);

#endif
