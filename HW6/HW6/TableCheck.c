//
//  TableCheck.c
//  HW6
//
//  Created by Cuong Dong on 4/14/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "Algebra.h"
#include "TableCheck.h"
#include "Util.h"

typedef enum {S, P} Line;

bool getLine(FILE *input, unsigned char *p, Line l){
	int length = 0;
	switch (l) {
		case S:
			length = 0x100;
			break;
		case P:
			length = 4;
			break;
		default:
			return false;
	}
	const int hexSize = 2 * length;
	char buffer[hexSize];
    int result = fscanf(input, l == S ? "%512[0123456789abcdef]" : "%8[0123456789abcdef]", buffer);
    if (result < 1){
        fprintf(stderr, "Unable to read values");
        return false;
    }
    
	for (int i = 0; i < hexSize; i++) {
		if (!isxdigit(buffer[i])){
			fprintf(stderr, "Invalid character found: '%c'. All characters must be a hex digit.\n", buffer[i]);
			return false;
		}
	}
	
	if (l == S){
		hexToByte(buffer, p, length);
	}
	else {
		hexToPoly(p, buffer);
	}
	
    return true;
}

//Check if the buffer is valid
bool checkBuffer(unsigned char *buffer, int bufferLength, int range, int min, int max){
    int count[256];
    for (int i = 0; i < range; i++) {
        count[i] = 0;
    }
    
    for (int i = 0; i < bufferLength; i++) {
        count[buffer[i]]++;
    }
    
    for (int i = 0; i < range; i++) {
        if ((count[i] < min) || (count[i] > max)) {
            fprintf(stderr,"Value %d appears %d times\n", i, count[i]);
            return false;
        }
    }
    return true;
}

bool parseP(FILE *input, unsigned char values[]){    
    bool valid = getLine(input, values, P);
    if (!valid) {
		fprintf(stderr, "(P) Error processing P.\n");
        return false;
    }
	
	return true;
}

bool parseInvp(FILE *input, unsigned char values[]){
    bool valid = getLine(input, values, P);
    if (!valid) {
		fprintf(stderr, "(INVP) Error processing INVP.\n");
        return false;
    }
	
	return true;
}

bool parseS(FILE *input, unsigned char values[]){
    bool valid = getLine(input, values, S);
    if (!valid) {
		fprintf(stderr, "(S) Error processing S.\n");
        return false;
    }
	
	valid = checkBuffer(values, 256, 256, 1, 1);
    if (!valid){
        fprintf(stderr, "(S) S is not a permutation.\n");
        return false;
    }
    else {
        return true;
    }
}


bool checkTable(FILE *input, unsigned char s[], unsigned char p[], unsigned char invp[]){
	char key[5];
    bool valid = true;
    int result = 0;
    for (int line = 0; line < NUMBER_OF_LINES; line++){
        result = fscanf(input, " %4[^=]=", key);
        if (result < 1){
            fprintf(stderr, "Unable to read key. Expecting S, P, or INVP.\n");
            return false;
        }
		
        if (strcmp(key, "S") == 0) {
            valid = parseS(input, s);
        } else if (strcmp(key, "P") == 0) {
            valid = parseP(input, p);
        } else if (strcmp(key, "INVP") == 0) {
            valid = parseInvp(input, invp);
        } else {
			fprintf(stderr, "Invalid key: %s. Expecting S, P, or INVP.\n", key);
			return false;
		}
		
        if (!valid) return false;
    }
	
	GF_2_8 prod;
	modProduct(p, invp, prod);
	valid = prod[0] == 1 && prod[1] == 0 && prod[2] == 0 && prod[3] == 0;
	if (!valid){
		fprintf(stderr, "(INVP) P * INVP must be 1 (modulo x^4 + 1).\n");
        return false;
	}
	
    return valid;
}
