//
//  Util.c
//  HW6
//
//  Created by Cuong Dong on 4/14/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

//Print the provided message to stderr and exit
void printError(const char *message) {
    fprintf(stderr, "%s\n", message);
    exit(1);
}

void fileError(const char *message) {
    perror(message);
    exit(1);
}

//Open a file with provided name as binary for writing
FILE* openOutputFile(const char * name){
    FILE *input;
    input = fopen((char*)name, "wb");
    //check error and exit early
    if (input == NULL){
        fileError("Cannot open the specified file");
    }
    return input;
}

//Open a file with provided name as binary for reading
FILE * openFile(const char *fileName) {
    FILE *input;
    input = fopen(fileName, "rb");
    //check error and exit early
    if (input == NULL){
        fileError("Cannot open the specified file");
    }
    return input;
}

void xorArray(unsigned char *l, unsigned char *r, int n, unsigned char *out){
	for (int i = 0; i < n; i++) {
		out[i] = l[i] ^ r[i];
	}
}

void hexToByte(const char in[], unsigned char out[], int length){
	for (int i = 0; i < length; i++){
		sscanf(in + (2 * i), "%2hhx", out + i);
	}
}

void printHex(unsigned char bin[], int length){
	for (int j = 0; j < length; j++) {
		printf("%02x", bin[j]);
	}
}
