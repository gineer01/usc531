//
//  AES.h
//  HW6
//
//  Created by Cuong Dong on 4/15/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW6_AES_h
#define HW6_AES_h

#define WORD_SIZE 4
#define Nk 4
#define Nr 10
#define Nb 4
#define NO_SUBKEYS 44

#include "Algebra.h"

typedef unsigned char Word[WORD_SIZE];

//Key expansion algorithm for AES
void keyExpansion(unsigned char key[], Word w[]);

//Set global variables used by AES
void initAes(unsigned char s[], GF_2_8 p, GF_2_8 invp);

//Encrypt/decrypt a file
int cryptFile(FILE *input, Word subkeys[], bool isEncrypt);

#endif
