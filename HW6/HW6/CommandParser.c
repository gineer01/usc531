//
//  CommandParser.c
//  HW6
//
//  Created by Cuong Dong on 4/14/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "CommandParser.h"

#define TABLE "t"
#define KEY "k"
#define POLY_1 "p1"
#define POLY_2 "p2"
#define POLY "p"

#define KEY_SIZE 16
#define POLY_SIZE 4

//Functions to parse command options after the command is known
CommandOptions parseTableCheck(int argc, char *argv[]);
CommandOptions parseModProd(int argc, char *argv[]);
CommandOptions parseKeyExpand(int argc, char *argv[]);
CommandOptions parseInverse(int argc, char *argv[]);
CommandOptions parseCrypt(Command c, int argc, char *argv[]);


//Print usage to user when an invalid command option is found and exit
void printUsage(){
    char * usage = "The commandline syntax for hw6 is as follows:\n"
    "hw6 tablecheck -t=tablefile\n"
    "hw6 modprod -p1=poly1 -p2=poly2\n"
    "hw6 keyexpand -k=key -t=tablefile\n"
    "hw6 encrypt -k=key -t=tablefile [file]\n"
    "hw6 decrypt -k=key -t=tablefile [file]\n"
    "hw6 inverse -p=poly\n";
    fprintf(stderr, "%s", usage);
    exit(1);
}

//Print an error message and then print the proper usage and exit
void printErrorAndUsage(const char *message) {
    fprintf(stderr, "%s", message);
    printUsage();
}

//A pair of key and value (e.g., p="password")
typedef struct {
    const char *option;
    const char *value;
} Pair;

//Parse a string of format <key>=<value> to a Pair
Pair getPair(char *argv){
    Pair pair = {"", ""};
    char *option = argv;
    for(char *value = argv; *value != 0; value++){
        if (*value == '='){
            *value = 0;
            value++;
            pair.option = option;
            pair.value = value;
            return pair;
        }
    }
    
    printErrorAndUsage("Wrong command option (missing =)");
    
    return pair;
}

//Parse the command and dispatch to the corresponding function
CommandOptions parseCommandLine(int argc, char *argv[]){
    if (argc <= 1) {
        printErrorAndUsage("Please provide a command\n");
    }
    
    if (strcmp(argv[1], "tablecheck") == 0) {
        return parseTableCheck(argc, argv);
    } else if (strcmp(argv[1], "encrypt") == 0) {
        return parseCrypt(ENCRYPT, argc, argv);
    } else if (strcmp(argv[1], "decrypt") == 0) {
        return parseCrypt(DECRYPT, argc, argv);
    } else if (strcmp(argv[1], "modprod") == 0) {
        return parseModProd(argc, argv);
    } else if (strcmp(argv[1], "keyexpand") == 0) {
        return parseKeyExpand(argc, argv);
    } else if (strcmp(argv[1], "inverse") == 0) {
        return parseInverse(argc, argv);
    }else {
        printErrorAndUsage("Invalid command \n");
    }
    
    exit(1);
}

//Parse the command options for tablecheck command
CommandOptions parseTableCheck(int argc, char *argv[]){
    CommandOptions options = {TABLECHECK, "", "", false, ""};
    if (argc == 3){
        for (int i = 2; i < 3; i++) {
            if (argv[i][0] == '-'){
                Pair pair = getPair(argv[i] + 1);
                if (strcmp(pair.option, TABLE) == 0){
                    options.tableFile = pair.value;
                }
                else {
                    printErrorAndUsage("Invalid command option.\n");
                }
            }
            else {
                printErrorAndUsage("Invalid command option (missing -) \n");
            }
        }
    }
    else {
        printErrorAndUsage("Invalid number of arguments \n");
    }
    
    return options;
}

void validateKey(const char *hexString, int expectedLength){
	size_t length = strlen(hexString);
	
	if (length != expectedLength){
		fprintf(stderr, "Invalid size. Hex string must be %d bytes long in hex format.", expectedLength);
		printErrorAndUsage("\n");
	}
	
	for (int i = 0; i < length; i++) {
		if (!isxdigit(hexString[i])){
			fprintf(stderr, "Invalid character found in hexstring: '%c'.", hexString[i]);
			printErrorAndUsage("\n");
		}
	}
}

//Parse the command options for crypt command
CommandOptions parseCrypt(Command c, int argc, char *argv[]){
    CommandOptions options = {c, "", "", false, ""};
    if (argc == 4){
        options.isStdin = true;
    }
    else if (argc == 5){
        options.isStdin = false;
        options.inputFile = argv[4];
    }
    else {
        printErrorAndUsage("Invalid number of arguments.\n");
    }
    
    for (int i = 2; i < 4; i++) {
        if (argv[i][0] == '-'){
            Pair pair = getPair(argv[i] + 1);
            if (strcmp(pair.option, KEY) == 0){
                validateKey(pair.value, KEY_SIZE * 2);
                options.key = pair.value;
            }
            else if (strcmp(pair.option, TABLE) == 0){
                options.tableFile = pair.value;
            }
            else {
                printErrorAndUsage("Invalid command option.\n");
            }
        }
        else {
            printErrorAndUsage("Invalid command option (missing -) \n");
        }
    }
    
    return options;
}

CommandOptions parseModProd(int argc, char *argv[]){
	CommandOptions options = {MODPROD, "", "", false, "", "", ""};
    if (argc != 4){
        printErrorAndUsage("Invalid number of arguments.\n");
    }
    
    for (int i = 2; i < 4; i++) {
        if (argv[i][0] == '-'){
            Pair pair = getPair(argv[i] + 1);
            if (strcmp(pair.option, "p1") == 0){
                validateKey(pair.value, POLY_SIZE * 2);
                options.poly1 = pair.value;
            }
            else if (strcmp(pair.option, "p2") == 0){
                validateKey(pair.value, POLY_SIZE * 2);
                options.poly2 = pair.value;
            }
            else {
                printErrorAndUsage("Invalid command option.\n");
            }
        }
        else {
            printErrorAndUsage("Invalid command option (missing -) \n");
        }
    }
    
    return options;
}

CommandOptions parseKeyExpand(int argc, char *argv[]){
	CommandOptions options = {KEYEXPAND, "", "", false, ""};
    if (argc != 4){
        printErrorAndUsage("Invalid number of arguments.\n");
    }
    
    for (int i = 2; i < 4; i++) {
        if (argv[i][0] == '-'){
            Pair pair = getPair(argv[i] + 1);
            if (strcmp(pair.option, KEY) == 0){
                validateKey(pair.value, KEY_SIZE * 2);
                options.key = pair.value;
            }
            else if (strcmp(pair.option, TABLE) == 0){
                options.tableFile = pair.value;
            }
            else {
                printErrorAndUsage("Invalid command option.\n");
            }
        }
        else {
            printErrorAndUsage("Invalid command option (missing -) \n");
        }
    }
    
    return options;
}

CommandOptions parseInverse(int argc, char *argv[]){
	CommandOptions options = {INVERSE, "", "", false, "", "", ""};
    if (argc != 3){
        printErrorAndUsage("Invalid number of arguments.\n");
    }
    
    for (int i = 2; i < 3; i++) {
        if (argv[i][0] == '-'){
            Pair pair = getPair(argv[i] + 1);
            if (strcmp(pair.option, "p") == 0){
                validateKey(pair.value, POLY_SIZE * 2);
                options.poly1 = pair.value;
            }            
            else {
                printErrorAndUsage("Invalid command option.\n");
            }
        }
        else {
            printErrorAndUsage("Invalid command option (missing -) \n");
        }
    }
    
    return options;
}



