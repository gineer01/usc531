//
//  Util.h
//  HW6
//
//  Created by Cuong Dong on 4/14/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW6_Util_h
#define HW6_Util_h

void xorArray(unsigned char *l, unsigned char *r, int n, unsigned char *out);

void hexToByte(const char in[], unsigned char out[], int length);

//Print the provided message to stderr and exit
void printError(const char *message);

void fileError(const char *message);

//Open a file with provided name as binary for writing
FILE* openOutputFile(const char * name);

//Open a file with provided name as binary for reading
FILE * openFile(const char *fileName);

void printHex(unsigned char bin[], int length);
#endif
