//
//  CommandParser.h
//  HW6
//
//  Created by Cuong Dong on 4/14/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW6_CommandParser_h
#define HW6_CommandParser_h

#include <stdbool.h>

//Enumeration of all possible commands
typedef enum {TABLECHECK, ENCRYPT, DECRYPT, MODPROD, KEYEXPAND, INVERSE} Command;

//struct to capture all possible command options
typedef struct {
    Command command;
    const char *key;
    const char *tableFile;
    bool isStdin;
    const char *inputFile;
	const char *poly1;
	const char *poly2;
} CommandOptions;

//Parse the command line argument and return CommandOptions struct
CommandOptions parseCommandLine(int argc, char *argv[]);

#endif
