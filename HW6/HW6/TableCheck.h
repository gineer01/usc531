//
//  TableCheck.h
//  HW6
//
//  Created by Cuong Dong on 4/14/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW6_TableCheck_h
#define HW6_TableCheck_h

#include <stdbool.h>
#include <stdio.h>

#define NUMBER_OF_LINES 3

//Read table file, parse line, and check
bool checkTable(FILE *input, unsigned char s[], unsigned char p[], unsigned char invp[]);

#endif
