//
//  Algebra.c
//  HW6
//
//  Created by Cuong Dong on 4/14/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <string.h>

#include "Algebra.h"
#include "Util.h"


Z_2 xTime(Z_2 b){
	const Z_2 modulo = 0x1B;
	if ((b & 0x80) != 0){
		return (b << 1) ^ modulo;
	}
	else {
		return b << 1;
	}
}

Z_2 dot(Z_2 a, Z_2 b){
	Z_2 temp = a;
	Z_2 result = 0;
	Z_2 v = b;
	for (int i = 0; i < 8; i++){
		if ((temp & 1) != 0){
			result ^= v;
		}
		v = xTime(v);
		temp = temp >> 1;
	}
	return result;
}

Z_2 inverses[] = {
	0x00,0x01,0x8d,0xf6,0xcb,0x52,0x7b,0xd1,0xe8,0x4f,0x29,0xc0,0xb0,0xe1,0xe5,0xc7,
    0x74,0xb4,0xaa,0x4b,0x99,0x2b,0x60,0x5f,0x58,0x3f,0xfd,0xcc,0xff,0x40,0xee,0xb2,
    0x3a,0x6e,0x5a,0xf1,0x55,0x4d,0xa8,0xc9,0xc1,0x0a,0x98,0x15,0x30,0x44,0xa2,0xc2,
    0x2c,0x45,0x92,0x6c,0xf3,0x39,0x66,0x42,0xf2,0x35,0x20,0x6f,0x77,0xbb,0x59,0x19,
    0x1d,0xfe,0x37,0x67,0x2d,0x31,0xf5,0x69,0xa7,0x64,0xab,0x13,0x54,0x25,0xe9,0x09,
    0xed,0x5c,0x05,0xca,0x4c,0x24,0x87,0xbf,0x18,0x3e,0x22,0xf0,0x51,0xec,0x61,0x17,
    0x16,0x5e,0xaf,0xd3,0x49,0xa6,0x36,0x43,0xf4,0x47,0x91,0xdf,0x33,0x93,0x21,0x3b,
    0x79,0xb7,0x97,0x85,0x10,0xb5,0xba,0x3c,0xb6,0x70,0xd0,0x06,0xa1,0xfa,0x81,0x82,
    0x83,0x7e,0x7f,0x80,0x96,0x73,0xbe,0x56,0x9b,0x9e,0x95,0xd9,0xf7,0x02,0xb9,0xa4,
    0xde,0x6a,0x32,0x6d,0xd8,0x8a,0x84,0x72,0x2a,0x14,0x9f,0x88,0xf9,0xdc,0x89,0x9a,
    0xfb,0x7c,0x2e,0xc3,0x8f,0xb8,0x65,0x48,0x26,0xc8,0x12,0x4a,0xce,0xe7,0xd2,0x62,
    0x0c,0xe0,0x1f,0xef,0x11,0x75,0x78,0x71,0xa5,0x8e,0x76,0x3d,0xbd,0xbc,0x86,0x57,
    0x0b,0x28,0x2f,0xa3,0xda,0xd4,0xe4,0x0f,0xa9,0x27,0x53,0x04,0x1b,0xfc,0xac,0xe6,
    0x7a,0x07,0xae,0x63,0xc5,0xdb,0xe2,0xea,0x94,0x8b,0xc4,0xd5,0x9d,0xf8,0x90,0x6b,
    0xb1,0x0d,0xd6,0xeb,0xc6,0x0e,0xcf,0xad,0x08,0x4e,0xd7,0xe3,0x5d,0x50,0x1e,0xb3,
    0x5b,0x23,0x38,0x34,0x68,0x46,0x03,0x8c,0xdd,0x9c,0x7d,0xa0,0xcd,0x1a,0x41,0x1c
};

//Find inverse by table lookup
Z_2 dotInverse(Z_2 a){
	int i = a;
	if (i == 0){
		printError("Division by zero. Cannot find inverse of 0. Program will exit!");
	}
	return inverses[i];
}

void modAdd(GF_2_8 a, GF_2_8 b, GF_2_8 result){
	for (int i = 0; i < POLY_SIZE; i++) {
		result[i] = a[i] ^ b[i];
	}
}

void modProduct(GF_2_8 a, GF_2_8 b, GF_2_8 result){
	result[0] = dot(a[0], b[0]) ^ dot(a[3], b[1]) ^ dot(a[2], b[2]) ^ dot(a[1], b[3]);
	result[1] = dot(a[1], b[0]) ^ dot(a[0], b[1]) ^ dot(a[3], b[2]) ^ dot(a[2], b[3]);
	result[2] = dot(a[2], b[0]) ^ dot(a[1], b[1]) ^ dot(a[0], b[2]) ^ dot(a[3], b[3]);
	result[3] = dot(a[3], b[0]) ^ dot(a[2], b[1]) ^ dot(a[1], b[2]) ^ dot(a[0], b[3]);
}

int getDegree(GF_2_8 a){
	for (int i = POLY_SIZE; i >= 1; i--) {
		if (a[i - 1] != 0) return i - 1;
	}
	
	return 0;
}

//Long division for polynomials GF(2^8)[x]
void modDiv(GF_2_8 a, GF_2_8 b, GF_2_8 quotient, GF_2_8 remainder, bool isModulo, bool isLast){
	int degA = isModulo ? 4 : getDegree(a);
	int degB = getDegree(b);
	
	unsigned char coA = isModulo ? 1 : a[degA];
	unsigned char coB = b[degB];
	
	if (degA == 0 && degB == 0 && isLast){
		quotient[0] = dot(a[0]^1, dotInverse(b[0]));
		remainder[0] = 1;
		return;
	}
	
	if (degA < degB) {
		for (int i = 0; i <= degA; i++){
			remainder[i] = a[i];
		}
		
		return;
	}
	
	int degQ = degA - degB;
	unsigned char q = quotient[degQ] = dot(dotInverse(coB), coA);
	GF_2_8 partial = {0, 0, 0, 0};
	for (int i = 0; i < degQ; i++) {
		partial[i] = a[i];
	}
	for (int i = degQ; i < degA; i++) {
		partial[i] = dot(q, b[i - degQ]) ^ a[i];
	}
	
	modDiv(partial, b, quotient, remainder, false, isLast);
}

bool isZero(GF_2_8 a){
	return a[0] == 0 && a[1] == 0 && a[2] == 0 && a[3] == 0;
}

bool eq(GF_2_8 a, GF_2_8 b){
	for (int i = 0; i < POLY_SIZE; i++) {
		if (a[i] != b[i]) return false;
	}
	return true;
}

void printEuclidTrace(int i, GF_2_8 rem, GF_2_8 quo, GF_2_8 aux){
	printf("i=%d, rem[i]=", i);
	printPoly(rem);
	printf(", quo[i]=");
	printPoly(quo);
	printf(", aux[i]=");
	printPoly(aux);
	printf("\n");
}

bool modInverse(GF_2_8 a, GF_2_8 inverse){
	GF_2_8 rem[6];
	GF_2_8 aux[6];
	GF_2_8 quo[6];
	
	GF_2_8 one = {1, 0, 0, 0};
	GF_2_8 zero = {0, 0, 0, 0};
	GF_2_8 temp;
	
	memcpy(rem[0], one, POLY_SIZE);
	memcpy(aux[0], zero, POLY_SIZE);
	memset(quo[0], 0, POLY_SIZE);
	printEuclidTrace(1, rem[0], quo[0], aux[0]);
	memcpy(rem[1], a, POLY_SIZE);
	memcpy(aux[1], one, POLY_SIZE);
	memset(quo[1], 0, POLY_SIZE);
	printEuclidTrace(2, rem[1], quo[1], aux[1]);
	int i = 1;
	while (!eq(rem[i], one)) {
		i++;
		memset(quo[i], 0, POLY_SIZE);
		memset(rem[i], 0, POLY_SIZE);
		modDiv(rem[i -2], rem[i - 1], quo[i], rem[i], i == 2, i == 5);
		modProduct(quo[i], aux[i - 1], temp);
		modAdd(temp, aux[i -2], aux[i]);
		printEuclidTrace(i + 1, rem[i], quo[i], aux[i]);
		
		if (isZero(rem[i])){
			return false;
		}
	}
	
	memcpy(inverse, aux[i], POLY_SIZE);
	return true;
}

/*
 Suppose A and B are two polynomials with coefficients in GF(2^8). The product C = A x B is given in the slide as C = M(A) x B, where M(A) is circulant matrix from A's coefficients.
 
 Let B be inverse of A^-1 and M^-1 be inverse of M(A). We have [1, 0, 0, 0] = A x A^-1 = M(A) x A^-1. Multiply both sides with M^-1 => M^-1 x [1, 0, 0, 0] = A^-1. Thus, we can get closed-form formula by inverting matrix M(A). WolframAlpha can help with inverting matrix
 
 http://www.wolframalpha.com/input/?i=inverse+{{a%2C+d%2C+c%2C+b}%2C+{b%2C+a%2C+d%2C+c}%2C+{c%2C+b%2C+a%2C+d}%2C+{d%2C+c%2C+b%2C+a}}
 
 */
bool modInverseByMatrix(GF_2_8 in, GF_2_8 result){
	unsigned char a = in[0];
	unsigned char b = in[1];
	unsigned char c = in[2];
	unsigned char d = in[3];
	
	unsigned char a2c2 = dot(a, a) ^ dot(c, c);
	unsigned char b2d2 = dot(b, b) ^ dot(d, d);
	
	unsigned char determinant = dot(a2c2, a2c2) ^ dot(b2d2, b2d2);
	if (determinant == 0) return false;
	unsigned char denom = dotInverse(determinant);
	
	result[0] = dot(denom, dot(a, a2c2) ^ dot(c, b2d2));
	result[1] = dot(denom, dot(b, a2c2) ^ dot(d, b2d2));
	result[2] = dot(denom, dot(c, a2c2) ^ dot(a, b2d2));
	result[3] = dot(denom, dot(d, a2c2) ^ dot(b, b2d2));

	return true;
}


void hexToPoly(GF_2_8 bytes, const char *hex) {
    for (int i = 0; i < POLY_SIZE; i++){
		sscanf(hex + (2 * i), "%2hhx", bytes + (POLY_SIZE - 1 - i));
	}
}

void printPoly(GF_2_8 poly){
	for (int i = 3; i >= 0 ; i--) {
		printf("{%02x}", poly[i]);
	}
}

