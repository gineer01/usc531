//
//  main.c
//  HW6
//
//  Created by Cuong Dong on 4/14/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include "CommandParser.h"
#include "Algebra.h"
#include "Util.h"
#include "TableCheck.h"
#include "AES.h"

//Functions to execute the selected command with the provided options
int doTableCheck(CommandOptions options);
int doCrypt(CommandOptions options, bool isEncrypt);
int doModProd(CommandOptions options);
int doKeyExpand(CommandOptions options);
int doInverse(CommandOptions options);


//Parse the command line arguments and dispatch to the corresponding function
int main(int argc, char * argv[]){
    CommandOptions options = parseCommandLine(argc, argv);
    switch(options.command){
        case TABLECHECK: return doTableCheck(options);
        case ENCRYPT: return doCrypt(options, true);
        case DECRYPT: return doCrypt(options, false);
        case MODPROD: return doModProd(options);
        case KEYEXPAND: return doKeyExpand(options);
		case INVERSE: return doInverse(options);
        default:
            return 0;
    }
}


int doTableCheck(CommandOptions options){
	FILE *input = openFile(options.tableFile);
	unsigned char s[0xff];
	unsigned char p[4];
	unsigned char invp[4];
    
    checkTable(input, s, p, invp);
    
    fclose(input);

	return 0;
}

int doCrypt(CommandOptions options, bool isEncrypt){
	FILE *tableFile = openFile(options.tableFile);
	unsigned char s[0xff];
	unsigned char p[4];
	unsigned char invp[4];
    
    bool valid = checkTable(tableFile, s, p, invp);
	if (!valid){
		fprintf(stderr, "Cannot proceed due to invalid table file.\n");
		fclose(tableFile);
		return 1;
	}
	
	initAes(s, p, invp);
	
	unsigned char key[4*Nk];
	hexToByte(options.key, key, 4*Nk);
    
	Word w[NO_SUBKEYS];
	keyExpansion(key, w);

	FILE *input = options.isStdin ? stdin : openFile(options.inputFile);
	
	cryptFile(input, w, isEncrypt);
	
    fclose(tableFile);
	if (!options.isStdin){
        fclose(input);
    }
	
	return 0;
}

int doModProd(CommandOptions options) {
	GF_2_8 p1;
	GF_2_8 p2;	
	hexToPoly(p1, options.poly1);
	hexToPoly(p2, options.poly2);
	
	GF_2_8 result;
	modProduct(p1, p2, result);
	printPoly(p1);
	printf(" CIRCLEX ");
	printPoly(p2);
	printf(" = ");
	printPoly(result);
	printf("\n");
	
	return 0;
}

int doKeyExpand(CommandOptions options){
	FILE *tableFile = openFile(options.tableFile);
	unsigned char s[0xff];
	unsigned char p[4];
	unsigned char invp[4];
    
    bool valid = checkTable(tableFile, s, p, invp);
	if (!valid){
		fprintf(stderr, "Cannot proceed due to invalid table file.\n");
		fclose(tableFile);
		return 1;
	}
	
	initAes(s, p, invp);
	
	unsigned char key[4*Nk];
	hexToByte(options.key, key, 4*Nk);
    
	Word w[NO_SUBKEYS];
	keyExpansion(key, w);
	
	for (int i = 0; i < NO_SUBKEYS; i++) {
		printf("w[%2d]: ", i);
		printHex(w[i], WORD_SIZE);
		printf("\n");
	}
	
    fclose(tableFile);

	return 0;
}

int doInverse(CommandOptions options) {
	GF_2_8 p;
	hexToPoly(p, options.poly1);
	
	GF_2_8 result;
	bool hasInverse = modInverse(p, result);
	if (hasInverse){
		printf("Multiplicative inverse of ");
		printPoly(p);
		printf(" is ");
		printPoly(result);
		printf("\n");
	}
	else {
		printPoly(p);
		printf(" does not have a multiplicative inverse.\n");
	}
	
	return 0;
}
