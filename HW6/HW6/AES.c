//
//  AES.c
//  HW6
//
//  Created by Cuong Dong on 4/15/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "AES.h"
#include "Util.h"

#define BUFFER_SIZE 16

unsigned char sbox[0x100];
unsigned char iSbox[0x100];
GF_2_8 mixColumnP;
GF_2_8 mixColumnInverseP;
bool isAesReady = false;

//Substition using sBox (or its inverse)
void substituteWord(unsigned char in[], unsigned char out[], int size, bool inverse){
	if (!isAesReady) {
		printError("AES is not ready. Init S-box for AES first");
	}
	for (int i = 0; i < size; i++) {
		out[i] = inverse? iSbox[in[i]] : sbox[in[i]];
	}
}

void initAes(unsigned char s[], GF_2_8 p, GF_2_8 invp){
	memcpy(sbox, s, 0x100);
	for (int i = 0; i < 0x100; i++) {
		iSbox[s[i]] = i;
	}
	
	memcpy(mixColumnP, p, POLY_SIZE);
	memcpy(mixColumnInverseP, invp, POLY_SIZE);
	isAesReady = true;
}

void keyExpansion(unsigned char key[], Word w[]){
	for (int i = 0; i < Nk; i++) {
		for (int j = 0; j < WORD_SIZE; j++) {
			w[i][j] = key[4*i + j];
		} 
	}

	unsigned char Rcon[] = {0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a};
	for (int i = Nk; i < NO_SUBKEYS; i++) {
		Word temp;
		memcpy(temp, w[i-1], WORD_SIZE);
		if (i % Nk == 0){
			Word rotated = {temp[1], temp[2], temp[3], temp[0]};
			substituteWord(rotated, temp, WORD_SIZE, false);
			Word rcon = {Rcon[i/Nk], 0, 0, 0};
			xorArray(temp, rcon, WORD_SIZE, temp);
		}
		xorArray(w[i - Nk], temp, WORD_SIZE, w[i]);
	}
}

void addRoundKey(Word state[Nb], Word subkeys[], int start){
	for (int i = 0; i < Nb; i++) {
		xorArray(state[i], subkeys[start + i], WORD_SIZE, state[i]);
	}
}

void subBytes(Word state[Nb]){
	for (int i = 0; i < Nb; i++) {
		substituteWord(state[i], state[i], WORD_SIZE, false);
	}
}

void invSubBytes(Word state[Nb]){
	for (int i = 0; i < Nb; i++) {
		substituteWord(state[i], state[i], WORD_SIZE, true);
	}
}

void shiftRows(Word state[Nb]){
	unsigned char row[Nb];
	for (int i = 1; i < WORD_SIZE; i++) {
		for (int j = 0; j < Nb; j++){
			row[j] = state[(j + i) % WORD_SIZE][i];
		}
		
		for (int j = 0; j < Nb; j++){
			state[j][i] = row[j];
		}
	}
}

void invShiftRows(Word state[Nb]){
	unsigned char row[Nb];
	for (int i = 1; i < WORD_SIZE; i++) {
		for (int j = 0; j < Nb; j++){
			row[j] = state[(j + Nb - i) % WORD_SIZE][i];
		}
		
		for (int j = 0; j < Nb; j++){
			state[j][i] = row[j];
		}
	}
}

void mixColumns(Word state[Nb]){
	Word temp;
	for (int i = 0; i < Nb; i++) {
		modProduct(mixColumnP, state[i], temp);
		memcpy(state[i], temp, WORD_SIZE);
	}
}

void invMixColumns(Word state[Nb]){
	Word temp;
	for (int i = 0; i < Nb; i++) {
		modProduct(mixColumnInverseP, state[i], temp);
		memcpy(state[i], temp, WORD_SIZE);
	}
}

typedef enum {INPUT, START, SBOX, SROW, MCOL, KEY, K_ADD, OUTPUT} Legend;

void printState(Word bin[], int round, Legend legend, bool isEncrypt){
	char *l;
	switch (legend) {
		case INPUT:
			l = "input";
			break;
		case START:
			l = "start";
			break;
		case SBOX:
			l = "s_box";
			break;
		case SROW:
			l = "s_row";
			break;
		case MCOL:
			l = "m_col";
			break;
		case KEY:
			l = "k_sch";
			break;
		case K_ADD:
			l = "k_add";
			break;
		case OUTPUT:
			l = "output";
			break;
		default:
			l = "wtf";
			break;
	}
	printf(isEncrypt ? "round[%2d].%-9s" : "round[%2d].i%-8s", round, l);
	printHex((unsigned char *)bin, BUFFER_SIZE);
	printf("\n");
}

//AES encrypt function
void encrypt(Word in[], Word out[], Word subkeys[]){
	Word state[Nb];
	for (int i = 0; i < Nb; i++){
		memcpy(state[i], in[i], WORD_SIZE);
	}
	
	printState(in, 0, INPUT, true);
	printState(subkeys + 0, 0, KEY, true);
	
	addRoundKey(state, subkeys, 0);
	
	int round;
	for (round = 1; round < Nr; round++){
		printState(state, round, START, true);
		subBytes(state);
		printState(state, round, SBOX, true);
		shiftRows(state);
		printState(state, round, SROW, true);
		mixColumns(state);
		printState(state, round, MCOL, true);
		addRoundKey(state, subkeys, round*Nb);
		printState(subkeys + round * Nb, round, KEY, true);
	}
	
	printState(state, round, START, true);
	subBytes(state);
	printState(state, round, SBOX, true);
	shiftRows(state);
	printState(state, round, SROW, true);
	addRoundKey(state, subkeys, Nr*Nb);
	printState(subkeys + round * Nb, round, KEY, true);
	
	for (int i = 0; i < Nb; i++){
		memcpy(out[i], state[i], WORD_SIZE);
	}
	printState(out, round, OUTPUT, true);
}

//AES decrypt function 
void decrypt(Word in[], Word out[], Word subkeys[]){
	Word state[Nb];
	for (int i = 0; i < Nb; i++){
		memcpy(state[i], in[i], WORD_SIZE);
	}
	
	printState(in, 0, INPUT, false);
	printState(subkeys + Nr * Nb, 0, KEY, false);
	
	addRoundKey(state, subkeys, Nr * Nb);
	
	int round;
	for (round = Nr - 1; round > 0; round--){
		printState(state, Nr - round, START, false);
		invShiftRows(state);
		printState(state, Nr - round, SROW, false);
		invSubBytes(state);
		printState(state, Nr - round, SBOX, false);
		
		addRoundKey(state, subkeys, round*Nb);
		printState(subkeys + round * Nb, Nr - round, KEY, false);
		printState(state, Nr - round, K_ADD, false);
		invMixColumns(state);	
	}
	
	printState(state, Nr - round, START, false);
	invShiftRows(state);
	printState(state, Nr - round, SROW, false);
	invSubBytes(state);
	printState(state, Nr - round, SBOX, false);
	addRoundKey(state, subkeys, 0);
	printState(subkeys + 0, Nr - round, KEY, false);
	
	for (int i = 0; i < Nb; i++){
		memcpy(out[i], state[i], WORD_SIZE);
	}
	printState(out, Nr - round, OUTPUT, false);
}

int cryptFile(FILE *input, Word subkeys[], bool isEncrypt){
	char buffer[BUFFER_SIZE];
    bool readFlag = true;//flag whether to continue to read another block
    int numberReadBytes;//save the number of bytes actually read when buffering
    
    if (input == NULL) {
        fprintf(stderr, "Null file pointer\n");
        return 1;
    }
    
	Word in[Nb];
	Word out[Nb];
	
	while (readFlag){
		numberReadBytes = (int)fread(buffer, sizeof(char), BUFFER_SIZE, input);
		if (numberReadBytes < BUFFER_SIZE){
			printError("File size less than 16 bytes. Program will exit");
		}
		readFlag = false;
		
		for (int i = 0; i < Nb; i++) {
			memcpy(in[i], buffer + (WORD_SIZE * i), WORD_SIZE);
		}
		
		if (isEncrypt){
			encrypt(in, out, subkeys);
		}
		else {
			decrypt(in, out, subkeys);
		}

//		fwrite(out, 1, BUFFER_SIZE, stdout);
	}
    
	return 0;
}
