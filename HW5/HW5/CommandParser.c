//
//  CommandParser.c
//  HW5
//
//  Created by Cuong Dong on 3/23/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "CommandParser.h"

#define TABLE "t"
#define KEY "k"

//Functions to parse command options after the command is known
CommandOptions parseTableCheck(int argc, char *argv[]);
CommandOptions parseCrypt(Command c, int argc, char *argv[]);


//Print usage to user when an invalid command option is found and exit
void printUsage(){
    char * usage = "The commandline syntax for hw5 is as follows:\n"
	"hw5 tablecheck -t=tablefile\n"
    "hw5 encrypt -k=key -t=tablefile [file]\n"
    "hw5 decrypt -k=key -t=tablefile [file]\n"
    "hw5 encrypt3 -k=key3 -t=tablefile [file]\n"
    "hw5 decrypt3 -k=key3 -t=tablefile [file]\n";
    fprintf(stderr, "%s", usage);
    exit(1);
}

//Print an error message and then print the proper usage and exit
void printErrorAndUsage(const char *message) {
    fprintf(stderr, "%s", message);
    printUsage();
}

//A pair of key and value (e.g., p="password")
typedef struct {
    const char *option;
    const char *value;
} Pair;

//Parse a string of format <key>=<value> to a Pair
Pair getPair(char *argv){
    Pair pair = {"", ""};
    char *option = argv;
    for(char *value = argv; *value != 0; value++){
        if (*value == '='){
            *value = 0;
            value++;
            pair.option = option;
            pair.value = value;
            return pair;
        }
    }
    
    printErrorAndUsage("Wrong command option (missing =)");
    
    return pair;
}

//Parse the command and dispatch to the corresponding function
CommandOptions parseCommandLine(int argc, char *argv[]){
    if (argc <= 1) {
        printErrorAndUsage("Please provide a command\n");
    }
    
    if (strcmp(argv[1], "tablecheck") == 0) {
        return parseTableCheck(argc, argv);
    } else if (strcmp(argv[1], "encrypt") == 0) {
        return parseCrypt(ENCRYPT, argc, argv);
    } else if (strcmp(argv[1], "decrypt") == 0) {
        return parseCrypt(DECRYPT, argc, argv);
    } else if (strcmp(argv[1], "encrypt3") == 0) {
        return parseCrypt(ENCRYPT3, argc, argv);
    } else if (strcmp(argv[1], "decrypt3") == 0) {
        return parseCrypt(DECRYPT3, argc, argv);
    } else {
        printErrorAndUsage("Invalid command \n");
    }
    
    exit(1);
}

//Parse the command options for tablecheck command
CommandOptions parseTableCheck(int argc, char *argv[]){
    CommandOptions options = {TABLECHECK, "", "", false, ""};
    if (argc == 3){
        for (int i = 2; i < 3; i++) {
            if (argv[i][0] == '-'){
                Pair pair = getPair(argv[i] + 1);
                if (strcmp(pair.option, TABLE) == 0){
                    options.tableFile = pair.value;
                }
                else {
                    printErrorAndUsage("Invalid command option.\n");
                }
            }
            else {
                printErrorAndUsage("Invalid command option (missing -) \n");
            }
        }
    }
    else {
        printErrorAndUsage("Invalid number of arguments \n");
    }
    
    return options;
}

void validateKey(Command c, const char *key){
	size_t length = strlen(key);
	for (int i = 0; i < length; i++) {
		if (!isxdigit(key[i])){
			fprintf(stderr, "Invalid character found in key: '%c'. Key must be a hex string.", key[i]);
			printErrorAndUsage("\n");
		}
	}
	
    size_t size = length/2;
    switch (c) {
        case ENCRYPT:
        case DECRYPT:
            if (size != KEY_SIZE){
                printErrorAndUsage("Invalid key size. Key must be 8 bytes long in hex format.\n");
            }
            break;
        case ENCRYPT3:
        case DECRYPT3:
            if (size != 3 * KEY_SIZE){
                printErrorAndUsage("Invalid key size. Key must be 24 bytes long in hex format.\n");
            }
        default:
            break;
    }
}

//Parse the command options for crypt command
CommandOptions parseCrypt(Command c, int argc, char *argv[]){
    CommandOptions options = {c, "", "", false, ""};
    if (argc == 4){
        options.isStdin = true;
    }
    else if (argc == 5){
        options.isStdin = false;
        options.inputFile = argv[4];
    }
    else {
        printErrorAndUsage("Invalid number of arguments.\n");
    }
    
    for (int i = 2; i < 4; i++) {
        if (argv[i][0] == '-'){
            Pair pair = getPair(argv[i] + 1);
            if (strcmp(pair.option, KEY) == 0){
                validateKey(c, pair.value);
                options.key = pair.value;                
            }
            else if (strcmp(pair.option, TABLE) == 0){
                options.tableFile = pair.value;
            }
            else {
                printErrorAndUsage("Invalid command option.\n");
            }
        }
        else {
            printErrorAndUsage("Invalid command option (missing -) \n");
        }
    }
    
    return options;
}
