//
//  main.c
//  HW5
//
//  Created by Cuong Dong on 3/22/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "CommandParser.h"
#include "DES.h"
#include "Util.h"
#include "TableCheck.h"

//Functions to execute the selected command with the provided options
int doTableCheck(CommandOptions options);
int doEncrypt(CommandOptions options);
int doDecrypt(CommandOptions options);
int doEncrypt3(CommandOptions options);
int doDecrypt3(CommandOptions options);

//Parse the command line arguments and dispatch to the corresponding function
int main(int argc, char * argv[]){
    CommandOptions options = parseCommandLine(argc, argv);
    switch(options.command){
        case TABLECHECK: return doTableCheck(options);
        case ENCRYPT: return doEncrypt(options);
        case DECRYPT: return doDecrypt(options);
        case ENCRYPT3: return doEncrypt3(options);
        case DECRYPT3: return doDecrypt3(options);
        default:
            return 0;
    }
}

int doTableCheck(CommandOptions options){
    FILE *input = openFile(options.tableFile);
    int ip[IP_LENGTH];
    int e[E_LENGTH];
    int p[P_LENGTH];
    int s[S_NUMBER][S_LENGTH];
    int v[V_LENGTH];
    int pc1[PC1_LENGTH];
    int pc2[PC2_LENGTH];    
    
    checkTable(input, ip, e, p, s, v, pc1, pc2);
    
    fclose(input);
	
	return 0;
}

void convertHexKey(char* key, const char *hexKey, int length) {
    for (int i = 0; i < length; i++){
		sscanf(hexKey + (2 * i), "%2hhx", key + i);
	}
}

int doEncrypt(CommandOptions options){
	FILE *tableFile = openFile(options.tableFile);
	
	int ip[IP_LENGTH];
    int e[E_LENGTH];
    int p[P_LENGTH];
    int s[S_NUMBER][S_LENGTH];
    int v[V_LENGTH];
    int pc1[PC1_LENGTH];
    int pc2[PC2_LENGTH];
    
    bool valid = checkTable(tableFile, ip, e, p, s, v, pc1, pc2);
	
	if (!valid){
		fprintf(stderr, "Cannot proceed due to invalid table file.\n");
		fclose(tableFile);
		return 1;
	}
	
    FILE *input = options.isStdin ? stdin : openFile(options.inputFile);
    
	//Convert key in hex to binary key
	char key[KEY_SIZE];
	const char *hexKey = options.key;
	convertHexKey(key, hexKey, KEY_SIZE);
	
    int result = encryptDes(input, ip, e, p, s, v, pc1, pc2, key);
    
    fclose(tableFile);
    if (!options.isStdin){
        fclose(input);
    }
    return result;
}
int doDecrypt(CommandOptions options){
	FILE *tableFile = openFile(options.tableFile);
	
	int ip[IP_LENGTH];
    int e[E_LENGTH];
    int p[P_LENGTH];
    int s[S_NUMBER][S_LENGTH];
    int v[V_LENGTH];
    int pc1[PC1_LENGTH];
    int pc2[PC2_LENGTH];
    
    bool valid = checkTable(tableFile, ip, e, p, s, v, pc1, pc2);
	
	if (!valid){
		fprintf(stderr, "Cannot proceed due to invalid table file.\n");
		fclose(tableFile);
		return 1;
	}
	
    FILE *input = options.isStdin ? stdin : openFile(options.inputFile);
    
	//Convert key in hex to binary key
	char key[KEY_SIZE];
	const char *hexKey = options.key;
	convertHexKey(key, hexKey, KEY_SIZE);
	
    int result = decryptDes(input, ip, e, p, s, v, pc1, pc2, key);
    
    fclose(tableFile);
    if (!options.isStdin){
        fclose(input);
    }
    return result;
}
int doEncrypt3(CommandOptions options){
	FILE *tableFile = openFile(options.tableFile);
	
	int ip[IP_LENGTH];
    int e[E_LENGTH];
    int p[P_LENGTH];
    int s[S_NUMBER][S_LENGTH];
    int v[V_LENGTH];
    int pc1[PC1_LENGTH];
    int pc2[PC2_LENGTH];
    
    bool valid = checkTable(tableFile, ip, e, p, s, v, pc1, pc2);
	
	if (!valid){
		fprintf(stderr, "Cannot proceed due to invalid table file.\n");
		fclose(tableFile);
		return 1;
	}
	
    FILE *input = options.isStdin ? stdin : openFile(options.inputFile);
    
	//Convert key in hex to binary key
	char key[3*KEY_SIZE];
	const char *hexKey = options.key;
	convertHexKey(key, hexKey, 3*KEY_SIZE);
	
    int result = encryptDes3(input, ip, e, p, s, v, pc1, pc2, key);
    
    fclose(tableFile);
    if (!options.isStdin){
        fclose(input);
    }
    return result;
}

int doDecrypt3(CommandOptions options){
	FILE *tableFile = openFile(options.tableFile);
	
	int ip[IP_LENGTH];
    int e[E_LENGTH];
    int p[P_LENGTH];
    int s[S_NUMBER][S_LENGTH];
    int v[V_LENGTH];
    int pc1[PC1_LENGTH];
    int pc2[PC2_LENGTH];
    
    bool valid = checkTable(tableFile, ip, e, p, s, v, pc1, pc2);
	
	if (!valid){
		fprintf(stderr, "Cannot proceed due to invalid table file.\n");
		fclose(tableFile);
		return 1;
	}
	
    FILE *input = options.isStdin ? stdin : openFile(options.inputFile);
    
	//Convert key in hex to binary key
	char key[3*KEY_SIZE];
	const char *hexKey = options.key;
	convertHexKey(key, hexKey, 3*KEY_SIZE);
	
    int result = decryptDes3(input, ip, e, p, s, v, pc1, pc2, key);
    
    fclose(tableFile);
    if (!options.isStdin){
        fclose(input);
    }
    return result;
}





