//
//  DES.h
//  HW5
//
//  Created by Cuong Dong on 3/23/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW5_DES_h
#define HW5_DES_h
#include "TableCheck.h"

int encryptDes(FILE *input, int ip[], int e[], int p[], int s[][S_LENGTH],
		   int v[], int pc1[], int pc2[], char *key);

int encryptDes3(FILE *input, int ip[], int e[], int p[], int s[][S_LENGTH],
			   int v[], int pc1[], int pc2[], char *key);

int decryptDes(FILE *input, int ip[], int e[], int p[], int s[][S_LENGTH],
			   int v[], int pc1[], int pc2[], char *key);

int decryptDes3(FILE *input, int ip[], int e[], int p[], int s[][S_LENGTH],
			   int v[], int pc1[], int pc2[], char *key);

#endif
