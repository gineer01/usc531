//
//  DES.c
//  HW5
//
//  Created by Cuong Dong on 3/23/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdlib.h>
#include <string.h>
#include "DES.h"
#include "Util.h"

#define HALF_SIZE 4
#define FULL_SIZE 8
#define KEY_SIZE 8
#define SUB_KEY_SIZE 6
#define SUB_KEY_NUMBER 16
#define T_LENGTH 7
#define C_D_SIZE 28
#define BUFFER_SIZE 8
#define TRIPLE 3

void xor(char *l, char *r, int n, char *out){
	for (int i = 0; i < n; i++) {
		out[i] = l[i] ^ r[i];
	}
}

void map(char *input, int mapping[], int length, char *output){
	for (int i = 0; i < length; i++) {
		setBit(output, i, getBit(input, mapping[i]));
	}
}

void inverseMap(char *input, int bijection[], int length, char *output){
	for (int i = 0; i < length; i++) {
		setBit(output, bijection[i], getBit(input, i));
	}
}

void substitute(char input[], int s[][S_LENGTH], char output[]){
	char b[S_NUMBER];
	for (int i = 0; i < 2; i++) {
		char oct1 = input[3 * i];
		char oct2 = input[3 * i + 1];
		char oct3 = input[3 * i + 2];
		b[i * 4] = (oct1 & 0xFC) >> 2;
		b[i * 4 + 1] = ((oct1 & 0x03) << 4) | ((oct2 & 0xF0) >> 4);
		b[i * 4 + 2] = ((oct2 & 0x0F) << 2) | ((oct3 & 0xC0) >> 6);
		b[i * 4 + 3] = (oct3 & 0x3F);
	}
	
	for (int i = 0; i < S_NUMBER; i++) {
		int row = ((b[i] & 0x20) >> 4) | (b[i] & 1);
		int col = (b[i] >> 1) & 0x0F;
		int val = s[i][row * S_GROUP_LENGTH + col];
		
		int mask = (i % 2 == 0) ? 0x0F : 0xF0;
		val = (i % 2 == 0) ? val << 4 : val;
		output[i/2] = (output[i/2] & mask) | val;
	}
}

//f = P(S(E(R) xor key))
void f(char R[HALF_SIZE], char key[], int p[], int s[][S_LENGTH], int e[], char output[HALF_SIZE]){
	char expanded[SUB_KEY_SIZE];
	map(R, e, E_LENGTH, expanded);//E(R)
	
	char xored[SUB_KEY_SIZE];
	xor(key, expanded, SUB_KEY_SIZE, xored);//E(R) xor key
	
	char substituted[HALF_SIZE];
	substitute(xored, s, substituted); //S(E(R) xor key)
	
	map(substituted, p, P_LENGTH, output);//P(S(E(R) xor key))
}

//output array must be different from input array
void Feistel(char input[FULL_SIZE], char key[], int p[], int s[][S_LENGTH], int e[], char output[FULL_SIZE]){
	char *inputL = input;
	char *inputR = input + HALF_SIZE;
	char *outputL = output;
	char *outputR = output + HALF_SIZE;
	
	//L_i = R_i-1
	memcpy(outputL, inputR, HALF_SIZE);
	//R_i = L_i-1 xor f(key, R_i-1)
	char temp[HALF_SIZE];
	f(inputR, key, p, s, e, temp);
	xor(inputL, temp, HALF_SIZE, outputR);
}

void printDebug(int iteration, char* L_R) {
	fprintf(stderr, "(L%d,R%d)=", iteration, iteration);
	printHex(stderr, L_R, FULL_SIZE);
	fprintf(stderr, "\n");
}

void desBlock(char plaintext[FULL_SIZE], char subkeys[SUB_KEY_NUMBER][SUB_KEY_SIZE], int ip[], int e[], int p[], int s[][S_LENGTH], char ciphertext[FULL_SIZE], bool debug){
	char temp[FULL_SIZE];
	
	map(plaintext, ip, IP_LENGTH, temp);
	
	int iteration = 0;
	if (debug){
		printDebug(iteration, temp);
	}
	
	char feistelOutput[FULL_SIZE];
	for (; iteration < SUB_KEY_NUMBER; iteration++) {
		Feistel(temp, subkeys[iteration], p, s, e, feistelOutput);
		
		if (debug){
			printDebug(iteration + 1, feistelOutput);
		}
		
		memcpy(temp, feistelOutput, FULL_SIZE);
	}
	
	if (iteration == SUB_KEY_NUMBER){//irregular swap
		char *L = feistelOutput;
		char *R = feistelOutput + HALF_SIZE;
		memcpy(temp, R, HALF_SIZE);
		memcpy(temp + HALF_SIZE, L, HALF_SIZE);
	}

	inverseMap(temp, ip, IP_LENGTH, ciphertext);
}

// ============ KEY SCHEDULING =================
//array length is C_D_SIZE, n is 1 or 2, so this doesn't check if n exceeds the array length
void rotate(char T[], int n){
	int last = (C_D_SIZE - n);
	char temp[2];
	//Rotate C
	for (int i = 0; i < n; i++) {
		setBit(temp, i, getBit(T, i));
	}
	for (int i = 0; i < last; i++) {
		setBit(T, i, getBit(T, i + n));
	}
	for (int i = last; i < C_D_SIZE; i++){
		setBit(T, i, getBit(temp, i - last));
	}
	
	//Rotate D
	for (int i = 0; i < n; i++) {
		setBit(temp, i, getBit(T, i + C_D_SIZE));
	}
	for (int i = 0; i < last; i++) {
		setBit(T, i + C_D_SIZE, getBit(T, i + C_D_SIZE + n));
	}
	for (int i = last; i < C_D_SIZE; i++){
		setBit(T, i + C_D_SIZE, getBit(temp, i - last));
	}
}

void printC_D(int iteration, char* T) {
	fprintf(stderr, "(C%d,D%d)=", iteration, iteration);
	printHex(stderr, T, T_LENGTH);
	fprintf(stderr, "\n");
}

bool isEqual(char subkey1[], char subkey2[]){
	bool result = true;
	for (int i = 0; i < SUB_KEY_SIZE; i++) {
		int a = subkey1[i];
		int b = subkey2[i];
		result = result && (a == b);
	}
	return result;
}

bool isAnti(char subkey1[], char subkey2[]){
	bool result = true;
	for (int i = 0; i < SUB_KEY_SIZE; i++) {
		int a = subkey1[i];
		int b = subkey2[i];
		result = result && (a == ~b);
	}
	return result;
}

bool isWeakKeys(char subkeys[SUB_KEY_NUMBER][SUB_KEY_SIZE]){
	bool isWeak = true;
	for (int i = 0; i < SUB_KEY_NUMBER/2; i++) {
		isWeak = isWeak && isEqual(subkeys[i], subkeys[SUB_KEY_NUMBER - 1 - i]);
	}
	if (isWeak) return true;
	
	bool isSemiWeak = true;
	for (int i = 0; i < SUB_KEY_NUMBER/2; i++) {
		isSemiWeak = isSemiWeak && isAnti(subkeys[i], subkeys[SUB_KEY_NUMBER - 1 - i]);
	}
	return isSemiWeak;
}

bool keyScheduling(char *key, int v[], int pc1[], int pc2[], char subkeys[SUB_KEY_NUMBER][SUB_KEY_SIZE]){
	char T[T_LENGTH];
	
	//C0 and D0
	map(key, pc1, PC1_LENGTH, T);
	
	//print to stderr
	int iteration = 0;
    printC_D(iteration, T);
	
	for (int i = 0; i < SUB_KEY_NUMBER; i++) {
		rotate(T, v[i]);
		map(T, pc2, PC2_LENGTH, subkeys[i]);
		
		iteration++;
		//print (Ci,Di)
		printC_D(iteration, T);
		//print subkey
		fprintf(stderr, "k%d=", iteration);
		printHex(stderr, subkeys[i], SUB_KEY_SIZE);
		fprintf(stderr, "\n");
	}
	
	//detect weak keys
	return isWeakKeys(subkeys);
}

void padZero(char* buffer, int nBytes) {
    if (nBytes < FULL_SIZE){
		for (int i = nBytes; i < FULL_SIZE; i++) {
			buffer[i] = 0;//pad 0
		}
	}
}

int desFile(FILE *input, int s[][S_LENGTH], int* p, int* e, int* ip, char subkeys[SUB_KEY_NUMBER][SUB_KEY_SIZE])
{
    char buffer[BUFFER_SIZE];
    bool readFlag = true;//flag whether to continue to read another block
    int numberReadBytes;//save the number of bytes actually read when buffering
    
    if (input == NULL) {
        fprintf(stderr, "Null file pointer\n");
        return 1;
    }
    
	char output[FULL_SIZE];
	bool isFirst = true;
	while (readFlag){
		numberReadBytes = (int)fread(buffer, sizeof(char), BUFFER_SIZE, input);
		if (numberReadBytes < BUFFER_SIZE){
			if (numberReadBytes == 0) return 0;
			
			readFlag = false;
			padZero(buffer, numberReadBytes);
		}
		
		desBlock(buffer, subkeys, ip, e, p, s, output, isFirst);
		
		isFirst = false;
		fwrite(output, 1, FULL_SIZE, stdout);
		//printHex(stdout, output, FULL_SIZE);
	}
    
	return 0;
}

int des3File(FILE *input, int s[][S_LENGTH], int* p, int* e, int* ip, char subkeys[][SUB_KEY_NUMBER][SUB_KEY_SIZE])
{
    char buffer[BUFFER_SIZE];
    bool readFlag = true;//flag whether to continue to read another block
    int numberReadBytes;//save the number of bytes actually read when buffering
    
    if (input == NULL) {
        fprintf(stderr, "Null file pointer\n");
        return 1;
    }
    
	char output[FULL_SIZE];
	bool isFirst = true;
	while (readFlag){
		numberReadBytes = (int)fread(buffer, sizeof(char), BUFFER_SIZE, input);
		if (numberReadBytes < BUFFER_SIZE){
			if (numberReadBytes == 0) return 0;
			
			readFlag = false;
			padZero(buffer, numberReadBytes);
		}
		
		desBlock(buffer, subkeys[0], ip, e, p, s, output, isFirst);
		desBlock(output, subkeys[1], ip, e, p, s, buffer, isFirst);
		desBlock(buffer, subkeys[2], ip, e, p, s, output, isFirst);
		
		isFirst = false;
		fwrite(output, 1, FULL_SIZE, stdout);
		//printHex(stdout, output, FULL_SIZE);
	}
    
	return 0;
}

int encryptDes(FILE *input, int ip[], int e[], int p[], int s[][S_LENGTH],
			   int v[], int pc1[], int pc2[], char *binaryKey){
	char subkeys[SUB_KEY_NUMBER][SUB_KEY_SIZE];
	bool isWeakKey = keyScheduling(binaryKey, v, pc1, pc2, subkeys);
	if (isWeakKey){
		fprintf(stderr, "The provided key is weak or semi-weak. This program will exit.");
		return 0;
	}
	
    return desFile(input, s, p, e, ip, subkeys);
}

void reverseKey(char subkeys[SUB_KEY_NUMBER][SUB_KEY_SIZE])
{
	//reverse keys
    char temp[SUB_KEY_SIZE];
	for (int i = 0; i < SUB_KEY_NUMBER/2; i++) {
		int j = SUB_KEY_NUMBER - 1 - i;
		memcpy(temp, subkeys[i], SUB_KEY_SIZE);
		memcpy(subkeys[i], subkeys[j], SUB_KEY_SIZE);
		memcpy(subkeys[j], temp,  SUB_KEY_SIZE);
	}
}

int decryptDes(FILE *input, int ip[], int e[], int p[], int s[][S_LENGTH],
			   int v[], int pc1[], int pc2[], char *binaryKey){
	char subkeys[SUB_KEY_NUMBER][SUB_KEY_SIZE];
	bool isWeakKey = keyScheduling(binaryKey, v, pc1, pc2, subkeys);
	if (isWeakKey){
		fprintf(stderr, "The provided key is weak or semi-weak. This program will exit.");
		return 0;
	}
	
    reverseKey(subkeys);
	
    return desFile(input, s, p, e, ip, subkeys);
}

int encryptDes3(FILE *input, int ip[], int e[], int p[], int s[][S_LENGTH],
				int v[], int pc1[], int pc2[], char *key){
	char subkeys[TRIPLE][SUB_KEY_NUMBER][SUB_KEY_SIZE];
	for (int i = 0; i < TRIPLE; i++) {
		bool isWeakKey = keyScheduling(key + i * KEY_SIZE, v, pc1, pc2, subkeys[i]);
		if (isWeakKey){
			fprintf(stderr, "One of the provided keys is weak or semi-weak. This program will exit.");
			return 0;
		}
	}
	
	reverseKey(subkeys[1]);
	
    return des3File(input, s, p, e, ip, subkeys);
}

int decryptDes3(FILE *input, int ip[], int e[], int p[], int s[][S_LENGTH],
				int v[], int pc1[], int pc2[], char *key){
	char subkeys[TRIPLE][SUB_KEY_NUMBER][SUB_KEY_SIZE];
	for (int i = 0; i < TRIPLE; i++) {
		bool isWeakKey = keyScheduling(key + i * KEY_SIZE, v, pc1, pc2, subkeys[TRIPLE - 1 - i]);
		if (isWeakKey){
			fprintf(stderr, "One of the provided keys is weak or semi-weak. This program will exit.");
			return 0;
		}
	}
	
	reverseKey(subkeys[0]);
	reverseKey(subkeys[2]);
	
    return des3File(input, s, p, e, ip, subkeys);
}

