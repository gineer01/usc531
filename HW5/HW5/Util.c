//
//  Util.c
//  HW5
//
//  Created by Cuong Dong on 3/23/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

//Print the provided message to stderr and exit
void printError(const char *message) {
    fprintf(stderr, "%s\n", message);
    exit(1);
}

void fileError(const char *message) {
    perror(message);
    exit(1);
}

//Open a file with provided name as binary for writing
FILE* openOutputFile(const char * name){
    FILE *input;
    input = fopen((char*)name, "wb");
    //check error and exit early
    if (input == NULL){
        fileError("Cannot open the specified file");
    }
    return input;
}

//Open a file with provided name as binary for reading
FILE * openFile(const char *fileName) {
    FILE *input;
    input = fopen(fileName, "rb");
    //check error and exit early
    if (input == NULL){
        fileError("Cannot open the specified file");
    }
    return input;
}

//Get a specific bit within a specific byte
int getBitInByte(char *stream, int byteLocation, int bitLocation){
    const unsigned char mask = 1 << (7 - bitLocation);
    if ((stream[byteLocation] & mask) == 0){
        return 0;
    }
    else return 1;
}

//Set n-th bit in buffer
void setBit(char buf[], int n, int set){
	int byteLocation = n / 8;
    int bitLocation = n % 8;
    const unsigned char mask = 1 << (7 - bitLocation);
    if (set){
        buf[byteLocation] = buf[byteLocation] | mask;
    }
    else {
        buf[byteLocation] = buf[byteLocation] & ~mask;
    }
}

int getBit(char buf[], int n){
	return getBitInByte(buf, n/8, n%8);
}

//Check if the buffer is valid
bool checkBuffer(int *buffer, int bufferLength, int range, int min, int max){
    int count[100];
    for (int i = 0; i < range; i++) {
        count[i] = 0;
    }
    
    for (int i = 0; i < bufferLength; i++) {
        count[buffer[i]]++;
    }
    
    for (int i = 0; i < range; i++) {
        if ((count[i] < min) || (count[i] > max)) {
            fprintf(stderr,"Value %d (or %d if zero-based index) appears %d times\n", i + 1, i, count[i]);
            return false;
        }
    }
    return true;
}

void printHex(FILE *output, char bin[], int length){
	for (int i = 0; i < length; i++) {
		unsigned char x = (unsigned char) bin[i];
		fprintf(output, "%02x", x);
	}
}
