//
//  Util.h
//  HW5
//
//  Created by Cuong Dong on 3/23/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW5_Util_h
#define HW5_Util_h
#include <stdbool.h>

//Print the provided message to stderr and exit
void printError(const char *message);

void fileError(const char *message);

//Open a file with provided name as binary for writing
FILE* openOutputFile(const char * name);

//Open a file with provided name as binary for reading
FILE * openFile(const char *fileName);

//Get a specific bit within a specific byte
int getBitInByte(char *stream, int byteLocation, int bitLocation);

//Get/Set n-th bit in buffer
void setBit(char buf[], int n, int set);
int getBit(char buf[], int n);

bool checkBuffer(int *buffer, int bufferLength, int n, int min, int max);

void printHex(FILE *output, char bin[], int length);

#endif
