//
//  CommandParser.h
//  HW5
//
//  Created by Cuong Dong on 3/23/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW5_CommandParser_h
#define HW5_CommandParser_h

#include <stdbool.h>
#define KEY_SIZE 8

//Enumeration of all possible commands
typedef enum {TABLECHECK, ENCRYPT, DECRYPT, ENCRYPT3, DECRYPT3} Command;

//struct to capture all possible command options
typedef struct {
    Command command;
    const char *key;
    const char *tableFile;
    bool isStdin;
    const char *inputFile;
} CommandOptions;

//Parse the command line argument and return CommandOptions struct
CommandOptions parseCommandLine(int argc, char *argv[]);

#endif
