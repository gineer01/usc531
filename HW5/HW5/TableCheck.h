//
//  TableCheck.h
//  HW5
//
//  Created by Cuong Dong on 3/25/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW5_TableCheck_h
#define HW5_TableCheck_h

#include <stdbool.h>
#include <stdio.h>

#define NUMBER_OF_LINES 14
#define IP_LENGTH 64
#define E_LENGTH 48
#define P_LENGTH 32
#define S_NUMBER 8
#define S_LENGTH 64
#define S_GROUP_LENGTH 16
#define S_GROUP 4
#define V_LENGTH 16
#define PC1_LENGTH 56
#define PC2_LENGTH 48

bool checkTable(FILE *input, int ip[], int e[], int p[],int s[S_NUMBER][S_LENGTH],
                int v[], int pc1[], int pc2[]);

#endif
