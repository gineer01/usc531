//
//  TableCheck.c
//  HW5
//
//  Created by Cuong Dong on 3/25/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include "TableCheck.h"
#include "Util.h"

bool getCsv(FILE *input, int max, int min, int* p, int length, bool convertToZeroIndex){
    int temp = 0;
    int result = fscanf(input, "%d", &temp);
    if (result < 1){
        fprintf(stderr, "Unable to read value.\n");
        return false;
    }
    if ((temp < min) || (temp > max)){
        fprintf(stderr, "Invalid value. Value must be %d to %d, inclusive.\n", min, max);
        return false;
    }
    p[0] = convertToZeroIndex ? temp - 1 : temp;
    
    for (int i = 1; i < length; i++){
        result = fscanf(input, ",%d", &temp);
        if (result < 1){
            fprintf(stderr, "Unable to read more value. Expecting %d values; %d values read.\n", length, i);
            return false;
        }
        if ((temp < min) || (temp > max)){
            fprintf(stderr, "Invalid value. Value must be %d to %d, inclusive.\n", min, max);
            return false;
        }
        p[i] = convertToZeroIndex ? temp - 1 : temp;
    }
    return true;
}

bool parseIP(FILE *input, int values[]){
    const int min = 1;
    const int max = 64;
    const int length = IP_LENGTH;
    
    bool valid = getCsv(input, max, min, values, length, true);
    if (!valid) {
		fprintf(stderr, "(IP) Error processing IP.\n");
        return false;
    }
    
    valid = checkBuffer(values, length, max, 1, 1);
    if (!valid){
        fprintf(stderr, "(IP) IP is not a permutation.\n");
        return false;
    }
    else {
        return true;
    }
}

bool parseE(FILE *input, int values[]){
    const int min = 1;
    const int max = 32;
    const int length = E_LENGTH;
    
    bool valid = getCsv(input, max, min, values, length, true);
    if (!valid) {
		fprintf(stderr, "(E) Error processing E.\n");
        return false;
    }
    
    valid = checkBuffer(values, length, max, 1, 2);
    if (!valid){
        fprintf(stderr, "(E) E is invalid. All 32 numbers must appear at least once and at most twice.\n");
        return false;
    }
    else {
        return true;
    }
}

bool parseP(FILE *input, int values[]){
    const int min = 1;
    const int max = 32;
    const int length = P_LENGTH;
    
    bool valid = getCsv(input, max, min, values, length, true);
    if (!valid) {
		fprintf(stderr, "(P) Error processing P.\n");
        return false;
    }
    
    valid = checkBuffer(values, length, max, 1, 1);
    if (!valid){
        fprintf(stderr, "(P) P is not a permutation.\n");
        return false;
    }
    else {
        return true;
    }
}

bool parseV(FILE *input, int values[]){
    const int min = 1;
    const int max = 2;
    const int length = V_LENGTH;
    
    bool valid = getCsv(input, max, min, values, length, false);
    if (!valid) {
		fprintf(stderr, "(V) Error processing V.\n");
        return false;
    }
    
    int count[3];
    for (int i = 0; i < 3; i++) {
        count[i] = 0;
    }
    
    for (int i = 0; i < length; i++) {
        count[values[i]]++;
    }
    
    valid = (count[1] == 4) && (count[2] == 12);
    if (!valid){
        fprintf(stderr, "(V) V is invalid. There should be 4 ones and 12 twos. There are %d ones and %d twos.\n", count[1], count[2]);
        return false;
    }
    else {
        return true;
    }
}

bool parsePC1(FILE *input, int values[]){
    const int min = 1;
    const int max = 64;
    const int length = PC1_LENGTH;
    
    bool valid = getCsv(input, max, min, values, length, true);
    if (!valid) {
		fprintf(stderr, "(PC1) Error processing PC1.\n");
        return false;
    }
	
    int count[max];
    for (int i = 0; i < max; i++) {
        count[i] = 0;
    }
    
    for (int i = 0; i < length; i++) {
        count[values[i]]++;
    }
    
    for (int i = 0; i < max; i++) {
        if ((i + 1) % 8 == 0){
            if (count[i] != 0){
                fprintf(stderr,"(PC1) Value %d should not be in PC1.\n", i + 1);
                return false;
            }
        }
        else if (count[i] != 1) {
            fprintf(stderr,"(PC1) Value %d appears %d times.\n", i + 1, count[i]);
            fprintf(stderr,"(PC1) PC1 is not a permutation.\n");
            return false;
        }
    }
    
    return true;
}

bool parsePC2(FILE *input, int values[]){
    const int min = 1;
    const int max = 56;
    const int length = PC2_LENGTH;
    
    bool valid = getCsv(input, max, min, values, length, true);
    if (!valid) {
		fprintf(stderr, "Error processing PC2.\n");
        return false;
    }
    
    int count[max];
    for (int i = 0; i < max; i++) {
        count[i] = 0;
    }
    
    for (int i = 0; i < length; i++) {
        count[values[i]]++;
    }
    
    int removed = 0;
    for (int i = 0; i < max; i++) {
        if (count[i] == 0){
            removed++;
        }
        else if (count[i] != 1) {
            fprintf(stderr,"(PC2) Value %d appears %d times.\n", i + 1, count[i]);
            fprintf(stderr,"(PC2) PC2 is not a permutation.\n");
            return false;
        }
    }
    
    if (removed != 8){
        fprintf(stderr,"(PC2) Only 8 values between 1 and 56 are moved. %d are removed.\n", removed);
        return false;
    }
    
    return true;
}

bool parseS(FILE *input, const char *key, int s[S_NUMBER][S_LENGTH]){
    int sNumber;
    int result = sscanf(key, "S%d", &sNumber);
    if (result < 1){
        fprintf(stderr, "Invalid key: %s\n", key);
        return false;
    }
    
    if ((sNumber < 1) || (sNumber > S_NUMBER)){
        fprintf(stderr, "(S) Invalid S#. S# must be S1 to S8.\n");
        return false;
    }
    
    int min = 0;
    int max = 15;
    int length = S_LENGTH;
    
    bool valid = getCsv(input, max, min, s[sNumber - 1], length, false);
    if (!valid) {
		fprintf(stderr, "(S) Error processing S%d.\n", sNumber);
        return false;
    }
    
    for (int i = 0; i < S_GROUP; i++) {
        valid = checkBuffer(s[sNumber - 1] + S_GROUP_LENGTH * i, S_GROUP_LENGTH, S_GROUP_LENGTH, 1, 1);
        if (!valid){
            fprintf(stderr, "(S) Group %d in S%d is not a permutation.\n", i + 1, sNumber);
            return false;
        }
    }
    
    return true;
}

bool checkTable(FILE *input, int ip[], int e[], int p[],int s[S_NUMBER][S_LENGTH],
                int v[], int pc1[], int pc2[]){
    char key[4];
    bool valid = true;
    int result = 0;
    for (int line = 0; line < NUMBER_OF_LINES; line++){
        result = fscanf(input, " %3[^=]=", key);
        if (result < 1){
            fprintf(stderr, "Unable to read key\n");
            return false;
        }
        if (strcmp(key, "IP") == 0) {
            valid = parseIP(input, ip);
        } else if (strcmp(key, "E") == 0) {
            valid = parseE(input, e);
        } else if (strcmp(key, "P") == 0) {
            valid = parseP(input, p);
        } else if (strcmp(key, "V") == 0) {
            valid = parseV(input, v);
        } else if (strcmp(key, "PC1") == 0) {
            valid = parsePC1(input, pc1);
        } else if (strcmp(key, "PC2") == 0) {
            valid = parsePC2(input, pc2);
        } else {
            valid = parseS(input, key, s);
        }
        if (!valid) return false;
    }
    return true;
}
