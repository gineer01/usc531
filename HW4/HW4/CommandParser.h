//
//  CommandParser.h
//  HW4
//
//  Created by Cuong Dong on 3/1/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW4_CommandParser_h
#define HW4_CommandParser_h

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

//Enumeration of all possible commands
typedef enum {KEYGEN, CRYPT, INVKEY, HISTO, SOLVE} COMMAND;

//struct to capture all possible command options
typedef struct {
    COMMAND command;
    int period;
    const char *keyFile;
    const char *inputFile;
    bool isStdin;
    int index;
    int maxPeriod;
} CommandOptions;

//Parse the command line argument and return CommandOptions struct
CommandOptions parseCommandLine(int argc, char *argv[]);

#endif
