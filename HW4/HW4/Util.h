//
//  Util.h
//  HW4
//
//  Created by Cuong Dong on 3/2/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW4_Util_h
#define HW4_Util_h

//Print the provided message to stderr and exit
void printError(const char *message);

void fileError(const char *message);

//Open a file with provided name as binary for writing
FILE* openOutputFile(const char * name);

//Open a file with provided name as binary for reading
FILE * openFile(const char *fileName);

long fileSize(FILE * file);

#endif
