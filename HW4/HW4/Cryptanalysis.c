//
//  Cryptanalysis.c
//  HW4
//
//  Created by Cuong Dong on 3/2/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "Cryptanalysis.h"
#include "Util.h"

#define BUFFER_SIZE 256

//Functions to convert from char to int and int to char
int charToIndex(char c){
    return c - 'a';
}

char indexToChar(int i){
    return i + 'a';
}

//Check if a char is in the alphabet ('a' to 'z')
bool isInAlphabet(unsigned char c){
    return (c >= 'a') && (c <= 'z');
}

//Check if each char in a string is valid according to function above
bool isValidString(char *s, int length){
    for (int i = 0; i < length; i++) {
        if (!isInAlphabet(s[i])){
            return false;
        }
    }
    return true;
}

//Check if the buffer is a valid permutation
bool checkPermutation(int *buffer, int n){
    bool count[n];
    for (int i = 0; i < n; i++) {
        count[i] = false;
    }
    for (int i = 0; i < n; i++) {
        if (count[buffer[i]]){
            return false;
        }
        else {
            count[buffer[i]] = true;
        }
    }
    for (int i = 0; i < n; i++) {
        if (!count[i]) {
            return false;
        }
    }
    return true;
}

//Invert a key
void invertKey(char * input, char * output){
    if (!isValidString(input, ALPHABET_SIZE)){
        printError("Invalid character found");
    }
    
    int perm[ALPHABET_SIZE];
    for (int i = 0; i < ALPHABET_SIZE; i++) {
        perm[i] = charToIndex(input[i]);
    }
    if (!checkPermutation(perm, ALPHABET_SIZE)){
        printError("Invalid permutation key");
    }
    
    for(int i = 0; i < ALPHABET_SIZE; i++){
        int mapping = charToIndex(input[i]);
        output[mapping] = indexToChar(i);
    }
}

//Collect histogram from buffer. Helper function for function below
int collectHistogramFromBuffer(unsigned char *buffer, int nBytes, int startingPos, int period, Histogram * hist){
    int i;
    for (i = startingPos; i < nBytes; i += period){
        unsigned char c = buffer[i];
        if (!isInAlphabet(c)){
            continue;
        }
        
        hist->total++;
        hist->count[charToIndex(c)]++;
    }
    return i - nBytes;
}

//Collect data from input file into the specified histogram
void collectHistogram(FILE * input, int period, int index, Histogram * hist){
    unsigned char buffer[BUFFER_SIZE];
    bool readFlag = true;//flag whether to continue to read another block
    int numberReadBytes;//save the number of bytes actually read when buffering
    int pos = index - 1;
    
    while (readFlag){
        numberReadBytes = (int)fread(buffer, sizeof(char), BUFFER_SIZE, input);
        if (numberReadBytes < BUFFER_SIZE)//reaching the last block
            readFlag = false;
        pos = collectHistogramFromBuffer(buffer, numberReadBytes, pos, period, hist);
    }
}

//A global buffer to store count so that the order function used to sort
//histogram can access it
int count[ALPHABET_SIZE];
int orderFunction(const void *c1, const void *c2){
    int i1 = charToIndex(*((char*)c1));
    int i2 = charToIndex(*((char*)c2));
    if (count[i1] == count[i2]){
        return i1 - i2;
    }
    else {
        return count[i2] - count[i1];
    }
}

//print the histogram to the output
void printHistogram(Histogram * hist, FILE * output){    
    //sort
    char order[ALPHABET_SIZE];
    memcpy(order, ALPHABET, ALPHABET_SIZE);
    for(int i = 0; i < ALPHABET_SIZE; i++){
        count[i] = hist->count[i];
    }
    qsort(order, ALPHABET_SIZE, sizeof(char), orderFunction);
    
    fprintf(output, "L=%d\n", hist->total);
    for (int i = 0; i < ALPHABET_SIZE; i++) {
        int c = order[i];
        int count = hist->count[charToIndex(c)];
        double percentage = count * 100/((double)hist->total);
        fprintf(output, "%c: %d (%.2f%%)\n", c, count, percentage);
    }
}

void swap(int *s, int i, int j) {
    int t = s[j];
    s[j] = s[i];
    s[i] = t;
}

//Generate a permutation in the buffer with the provided key using
// RC4 key scheduling algorithm
void rc4Ksa(char * key, char * buffer){
    int s[ALPHABET_SIZE];
    size_t keyLength = strlen(key);
    for (int i = 0; i < ALPHABET_SIZE; i++) {
        s[i] = i;
    }
    int j = 0;
    for (int i = 0; i < ALPHABET_SIZE; i++) {
        j = (j + s[i] + key[i % keyLength]) % ALPHABET_SIZE;
        swap(s, i, j);
    }
    for (int i = 0; i < ALPHABET_SIZE; i++) {
        buffer[i] = indexToChar(s[i]);
    }
}

//Encrypt a character. Helper function for encrypt buffer below
char encrypt(unsigned char c, char keys[][KEY_SIZE], int keyIndex){
    if (!isInAlphabet(c)){
        return c;
    }
    else {
        return keys[keyIndex][charToIndex(c)];
    }
}

//Encrypt a buffer. Helper function for encrypt file function below
int encryptBuffer(char *buffer, int nBytes, char keys[][KEY_SIZE], int period, int startingPos){
    int keyIndex = startingPos;
    int count = 0;
    char output[BUFFER_SIZE];
    for (int i = 0; i < nBytes; i++){
        char c = buffer[i];
        if ((c != '\n') && ((c < 0x20) || (c > 0x7e))){
            fprintf(stderr, "Invalid character while reading input: %x", (unsigned char)c);
            exit(1);
        }
        
        output[i] = encrypt(c, keys, keyIndex);
        keyIndex = (keyIndex + 1) % period;
        count++;
        if (c == '\n') break;//problem statement assumes there is only one line from input
    }
    fwrite(output, sizeof(char), count, stdout);
    return keyIndex;
}

//Encrypt the input file with the provided keys and specified period
void vigenereCipher(FILE * input, char keys[][KEY_SIZE], int period){
    char buffer[BUFFER_SIZE];
    bool readFlag = true;//flag whether to continue to read another block
    int numberReadBytes;//save the number of bytes actually read when buffering
    int pos = 0;
    
    while (readFlag){
        numberReadBytes = (int)fread(buffer, sizeof(char), BUFFER_SIZE, input);
        if (numberReadBytes < BUFFER_SIZE)//reaching the last block
            readFlag = false;
        pos = encryptBuffer(buffer, numberReadBytes, keys, period, pos);
    }
}

//Read the file to get the period and validate the keys
int getPeriod(FILE * file){    
    int period = 0;
    char key[KEY_SIZE];
    int perm[ALPHABET_SIZE];
    while (fscanf(file, " %27s ", key) == 1){//1 is the number of conversion
        period++;
        for (int i = 0; i < ALPHABET_SIZE; i++) {
            perm[i] = charToIndex(key[i]);
        }
        if (!checkPermutation(perm, ALPHABET_SIZE)){
            printError("Invalid permutation in key file");
        }
    };
    
    int result = fseek(file, 0, SEEK_SET);
    if (result < 0){
        fileError("fseek failed");
    }
    
    return period;
}

//Find all matches of the specified length
//Return false if it cannot find a match, true otherwise
bool findMatch(char *buffer, long size, int length){
    char *ngramA;
    char *ngramB;
    char foundMatch[length + 1];
    foundMatch[length] = 0;
    
    bool found = false;
    
    long maxI = size - length;
    long maxJ = maxI + 1;
    for (int i = 0; i < maxI; i++) {
        ngramA = buffer + i;
        for (int j = i + 1; j < maxJ; j++) {            
            ngramB = buffer + j;
            if ((strncmp(ngramA, ngramB, length) == 0) && isValidString(ngramA, length)){
                found = true;
                memcpy(foundMatch, ngramA, length);
                printf("len=%d, i=%d, j=%d, j-i=%d, %s\n", length, i, j, j - i, foundMatch);
            }
        }
    }
    
    return found;
}

//Do Kasiski method and output to STDOUT
void doKasiski(char *buffer, long size){  
    bool found = false;
    printf("Kasiski Method\n"
           "==============\n");
    for (int length = 4; true; length++){
        found = findMatch(buffer, size, length);
        if (!found) {
            printf("len=%d, no more matches\n\n", length);
            return;
        }
    }
}

//Count frequencies of the letters in buffer into count[] array
//Return the total number of letters in the buffer
int countFrequency(char *buffer, long size, int count[]){
    for (int i = 0; i < ALPHABET_SIZE; i++){
        count[i] = 0;
    }
    int l = 0;
    for (int i = 0; i < size; i++) {
        char c = buffer[i];
        if (isInAlphabet(c)){
            l++;
            count[charToIndex(c)]++;
        }
    }
    
    return l;
}

//Calculate the index of coincidence from length and frequencies of letters
double getIc(int l, int f[]){
    double denom = l * (l - 1.0);
    int num = 0;
    for (int i = 0; i < ALPHABET_SIZE; i++){
        num += f[i] * (f[i] - 1);
    }
    return num/denom;
}

//The formula for expected index of coincidence
// l is length of cipher, t is the period
double getEic(double l, double t){
    double KP = 0.0658;
    double KR = 1/26.0;
    return ((l -t) * KP + (t - 1) * l * KR) / (t * (l - 1));
}

//Do index of coincidence and output to STDOUT
void doIndexOfCoincidence(char *buffer, long size, int maxPeriod){
    printf("Average Index of Coincidence\n"
           "============================\n");
    
    int count[ALPHABET_SIZE];
    int l = countFrequency(buffer, size, count);
    
    printf("L=%d\n", l);
    for (int i = 0; i < ALPHABET_SIZE; i++){
        printf("f('%c')=%d\n", indexToChar(i), count[i]);
    }
    
    printf("IC=%.8g\n", getIc(l, count));
    
    for (int i = 0; i < maxPeriod; i++) {
        int t = i + 1;
        printf("t=%d, E(IC)=%.8g\n", t, getEic(l, t));
    }
    printf("\n");
}

//Do auto-correlation and output to STDOUT
void doAutoCorrelation(char *buffer, long size, int maxPeriod){
    printf("Auto-correlation Method\n"
           "=======================\n");
    
    for (int t = 1; t <= maxPeriod; t++) {
        int count = 0;
        for (int i = 0; i < size - t; i++) {
            if ((isInAlphabet(buffer[i])) && (buffer[i] == buffer[i + t])){
                count++;
            }
        }
        printf("t=%d, count=%d\n", t, count);
    }
    
    printf("\n");
}
