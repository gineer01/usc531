//
//  main.c
//  HW4
//
//  Created by Cuong Dong on 3/1/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include "CommandParser.h"
#include "Util.h"
#include "Cryptanalysis.h"
#include <stdlib.h>
#include <time.h>

//Functions to execute the selected command with the provided options
int doKeygen(CommandOptions options);
int doCrypt(CommandOptions options);
int doInverseKey(CommandOptions options);
int doHistogram(CommandOptions options);
int doSolve(CommandOptions options);

//Parse the command line arguments and dispatch to the corresponding function
int main(int argc, char * argv[]){
    CommandOptions options = parseCommandLine(argc, argv);
    switch(options.command){
        case KEYGEN: return doKeygen(options);
        case CRYPT: return doCrypt(options);
        case INVKEY: return doInverseKey(options);
        case HISTO: return doHistogram(options);
        case SOLVE: return doSolve(options);
        default:
            return 0;
    }
}

int doKeygen(CommandOptions options){
    srand((unsigned int)time(NULL));
    char key[KEY_SIZE];
    sprintf(key, "%d", rand());    
    
    char buffer[KEY_SIZE];
    buffer[KEY_SIZE - 1] = 0;
    
    for (int i = 0; i < options.period; i++) {
        rc4Ksa(key, buffer);
        printf("%s\n", buffer);
        memcpy(key, buffer, KEY_SIZE);
    }
    
    return 0;
}


int doCrypt(CommandOptions options){
    FILE *keyFile = openFile(options.keyFile);
    int guess = getPeriod(keyFile);
    char keys[guess][KEY_SIZE];
    
    int period = 0;
    while (fscanf(keyFile, " %27s ", keys[period]) == 1){//1 is the number of conversion
        period++;
    };
    
    FILE *input = options.isStdin ? stdin : openFile(options.inputFile);
    
    vigenereCipher(input, keys, period);
    
    fclose(keyFile);
    if (!options.isStdin){
        fclose(input);
    }
    return 0;
}

int doInverseKey(CommandOptions options){
    FILE *input = openFile(options.keyFile);
    char iBuffer[KEY_SIZE];
    char oBuffer[KEY_SIZE];
    oBuffer[KEY_SIZE - 1] = 0;
    
    while (fscanf(input, " %27s ", iBuffer) == 1){//1 is the number of conversion
        invertKey(iBuffer, oBuffer);
        printf("%s\n", oBuffer);
    };    
    
    fclose(input);
    return 0;
}

int doHistogram(CommandOptions options){
    FILE *input = options.isStdin ? stdin : openFile(options.inputFile);
    FILE *output = stdout;
    
    Histogram hist;
    hist.total = 0;
    for (int i = 0; i < ALPHABET_SIZE; i++) {
        hist.count[i] = 0;
    }
    
    collectHistogram(input, options.period, options.index, &hist);
    printHistogram(&hist, output);
    
    if (!options.isStdin){
        fclose(input);
    }
    return 0;
}

int doSolve(CommandOptions options){
    FILE *input = openFile(options.inputFile);
    
    long size = fileSize(input);
    char buffer[size];
    
    size_t readBytes = fread(buffer, sizeof(char), size, input);
    if (readBytes < size){
        fileError("Error reading file");
    }
    
    doKasiski(buffer, size);
    
    doIndexOfCoincidence(buffer, size, options.maxPeriod);
    
    doAutoCorrelation(buffer, size, options.maxPeriod);
    
    fclose(input);
    return 0;
}
