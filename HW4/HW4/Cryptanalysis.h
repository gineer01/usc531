//
//  Cryptanalysis.h
//  HW4
//
//  Created by Cuong Dong on 3/2/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW4_Cryptanalysis_h
#define HW4_Cryptanalysis_h

#define ALPHABET "abcdefghijklmnopqrstuvwxyz"
#define ALPHABET_SIZE 26
#define KEY_SIZE 27

#include <stdbool.h>

void invertKey(char * input, char * output);

typedef struct {
    int total;
    int count[26];
} Histogram;

void collectHistogram(FILE * input, int period, int index, Histogram * hist);

void printHistogram(Histogram * hist, FILE * output);

void rc4Ksa(char * key, char * buffer);

void vigenereCipher(FILE * input, char keys[][KEY_SIZE], int period);

int getPeriod(FILE * file);

bool checkPermutation(int *buffer, int n);

void doKasiski(char *buffer, long size);

void doIndexOfCoincidence(char *buffer, long size, int maxPeriod);

void doAutoCorrelation(char *buffer, long size, int maxPeriod);

#endif
