//
//  Util.c
//  HW4
//
//  Created by Cuong Dong on 3/2/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

//Print the provided message to stderr and exit
void printError(const char *message) {
    fprintf(stderr, "%s\n", message);
    exit(1);
}

void fileError(const char *message) {
    perror(message);
    exit(1);
}

//Open a file with provided name as binary for writing
FILE* openOutputFile(const char * name){
    FILE *input;
    input = fopen((char*)name, "wb");
    //check error and exit early
    if (input == NULL){
        fileError("Cannot open the specified file");
    }
    return input;
}

//Open a file with provided name as binary for reading
FILE * openFile(const char *fileName) {
    FILE *input;
    input = fopen(fileName, "rb");
    //check error and exit early
    if (input == NULL){
        fileError("Cannot open the specified file");
    }
    return input;
}

long fileSize(FILE * file){
    int result = fseek(file, 0, SEEK_END);
    if (result < 0){
        fileError("fseek failed");
    }
    long size = ftell(file);
    if (size < 0){
        fileError("ftell failed");
    }
    result = fseek(file, 0, SEEK_SET);
    if (result < 0){
        fileError("fseek failed");
    }
    return size;
}

