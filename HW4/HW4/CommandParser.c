//
//  CommandParser.c
//  HW4
//
//  Created by Cuong Dong on 3/1/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include "CommandParser.h"

#define PERIOD "t"
#define KEY_FILE "k"
#define MAX_PERIOD "l"
#define INDEX "i"

//Functions to parse command options after the command is known
CommandOptions parseKeygen(int argc, char *argv[]);
CommandOptions parseInverseKey(int argc, char *argv[]);
CommandOptions parseEncrypt(int argc, char *argv[]);
CommandOptions parseHistogram(int argc, char *argv[]);
CommandOptions parseSolve(int argc, char *argv[]);



//Print usage to user when an invalid command option is found and exit
void printUsage(){
    char * usage = "\nThe commandline syntax for hw4 is as follows:\n"
        "hw4 keygen -t=period\n"
        "hw4 crypt -k=keyfile [file]\n"
        "hw4 invkey keyfile\n"
        "hw4 histo -t=period -i=which [file]\n"
        "hw4 solve -l=max_t file\n";
    fprintf(stderr, "%s", usage);
    exit(1);
}

//Print an error message and then print the proper usage and exit
void printErrorAndUsage(const char *message) {
    fprintf(stderr, "%s", message);
    printUsage();
}

//A pair of key and value (e.g., p="password")
typedef struct {
    const char *key;
    const char *value;
} Pair;

//Parse a string of format <key>=<value> to a Pair
Pair getPair(char *argv){
    Pair pair = {"", ""};
    char *key = argv;
    for(char *value = argv; *value != 0; value++){
        if (*value == '='){
            *value = 0;
            value++;
            pair.key = key;
            pair.value = value;
            return pair;
        }
    }
    
    printErrorAndUsage("Wrong command option (missing =)");
    
    return pair;
}

//Parse the command and dispatch to the corresponding function
CommandOptions parseCommandLine(int argc, char *argv[]){
    if (argc <= 1) {
        printErrorAndUsage("Please provide a command\n");
    }
    
    if (strcmp(argv[1], "keygen") == 0) {
        return parseKeygen(argc, argv);
    } else if (strcmp(argv[1], "invkey") == 0) {
        return parseInverseKey(argc, argv);
    } else if (strcmp(argv[1], "crypt") == 0) {
        return parseEncrypt(argc, argv);
    } else if (strcmp(argv[1], "histo") == 0) {
        return parseHistogram(argc, argv);
    } else if (strcmp(argv[1], "solve") == 0) {
        return parseSolve(argc, argv);
    } else {
        printErrorAndUsage("Invalid command \n");
    }
    
    exit(1);
}

//Parse the command options for keygen command
CommandOptions parseKeygen(int argc, char *argv[]){
    CommandOptions options = {KEYGEN, 0, "", "", false, 0, 0};
    if (argc == 3){
        for (int i = 2; i < 3; i++) {
            if (argv[i][0] == '-'){
                Pair pair = getPair(argv[i] + 1);
                if (strcmp(pair.key, PERIOD) == 0){
                    options.period = atoi(pair.value);
                    if (options.period < 1){
                        printErrorAndUsage("Period must be positive.\n");
                    }
                }
                else {
                    printErrorAndUsage("Invalid command option.\n");
                }
            }
            else {
                printErrorAndUsage("Invalid command option (missing -) \n");
            }
        }
    }
    else {
        printErrorAndUsage("Invalid number of arguments \n");
    }
    
    return options;
}

//Parse the command options for keygen command
CommandOptions parseHistogram(int argc, char *argv[]){
    CommandOptions options = {HISTO, 0, "", "", false, 0, 0};
    if (argc == 4){
        options.isStdin = true;
    }
    else if (argc == 5){
        options.isStdin = false;
        options.inputFile = argv[4];
    }
    else {
        printErrorAndUsage("Invalid number of arguments.\n");
    }
    
    for (int i = 2; i < 4; i++) {
        if (argv[i][0] == '-'){
            Pair pair = getPair(argv[i] + 1);
            if (strcmp(pair.key, PERIOD) == 0){
                options.period = atoi(pair.value);
                if (options.period < 1){
                    printErrorAndUsage("Period must be positive.\n");
                }
            }
            else if (strcmp(pair.key, INDEX) == 0){
                options.index = atoi(pair.value);
                if (options.index < 1){
                    printErrorAndUsage("Index must be between 1 and period (one-based index).\n");
                }
            }
            else {
                printErrorAndUsage("Invalid command option.\n");
            }
        }
        else {
            printErrorAndUsage("Invalid command option (missing -) \n");
        }
    }
    
    if (options.index > options.period){
        printErrorAndUsage("Index must be between 1 and period (one-based index).\n");
    }
    
    return options;
}

//Parse the command options for crypt command
CommandOptions parseEncrypt(int argc, char *argv[]){
    CommandOptions options = {CRYPT, 0, "", "", false, 0, 0};
    if (argc == 3){
        options.isStdin = true;
    }
    else if (argc == 4){
        options.isStdin = false;
        options.inputFile = argv[3];
    }
    else {
        printErrorAndUsage("Invalid number of arguments.\n");
    }
    
    for (int i = 2; i < 3; i++) {
        if (argv[i][0] == '-'){
            Pair pair = getPair(argv[i] + 1);
            if (strcmp(pair.key, KEY_FILE) == 0){
                options.keyFile = pair.value;
            }
            else {
                printErrorAndUsage("Invalid command option.\n");
            }
        }
        else {
            printErrorAndUsage("Invalid command option (missing -) \n");
        }
    }
    
    return options;
}

//Parse the command options for keygen command
CommandOptions parseSolve(int argc, char *argv[]){
    CommandOptions options = {SOLVE, 0, "", "", false, 0, 0};
    if (argc == 4){
        options.inputFile = argv[3];
    }
    else {
        printErrorAndUsage("Invalid number of arguments.\n");
    }
    
    for (int i = 2; i < 3; i++) {
        if (argv[i][0] == '-'){
            Pair pair = getPair(argv[i] + 1);
            if (strcmp(pair.key, MAX_PERIOD) == 0){
                options.maxPeriod = atoi(pair.value);
                if (options.maxPeriod < 1){
                    printErrorAndUsage("Max period must be positive.\n");
                }
            }
            else {
                printErrorAndUsage("Invalid command option.\n");
            }
        }
        else {
            printErrorAndUsage("Invalid command option (missing -) \n");
        }
    }
    
    return options;
}

//Parse the command options for invkey command
CommandOptions parseInverseKey(int argc, char *argv[]){
    CommandOptions options = {INVKEY, 0, "", "", false, 0, 0};
    if (argc == 3){
        options.keyFile = argv[2];
    }
    else {
        printErrorAndUsage("Invalid number of arguments \n");
    }
    return options;
}
