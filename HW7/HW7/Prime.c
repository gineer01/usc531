//
//  Prime.c
//  HW7
//
//  Created by Cuong Dong on 4/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include "Prime.h"
#include "Util.h"


void printBigEndian(uint32_t i) {
    uint32_t bigEndian = htonl(i);
    fwrite(&bigEndian, sizeof(uint32_t), 1, stdout);
}

uint32_t readBigEndian(FILE *primeFile) {
    uint32_t bigEndian;
    size_t numberReadBytes = fread(&bigEndian, sizeof(uint32_t), 1, primeFile);
	if (numberReadBytes == 1){
		return ntohl(bigEndian);
	}
	else {
		fileError("Error reading prime file\n");
		return 0;
	}
}

uint32_t readPrimeFile(uint32_t primes[], long count, FILE *primeFile){
	uint32_t maxVal = readBigEndian(primeFile);

	for (int i = 0; i < count; i++) {
		primes[i] = readBigEndian(primeFile);
	}
	
	return maxVal;
}

//Print all primes not greater than n to STDOUT in prime file format
// using sieve of Eratosthenes algorithm
void sieveOfEratosthenes(uint32_t n){
	const int array_size = n/8 + 1;
	char sieve[array_size];
	memset(sieve, 0xFF, array_size);
	
	setBit(sieve, 1, false);
	int p = 2;
	while (p * p <= n){
		int j = p * p;
		while (j <= n){
			setBit(sieve, j, false);
			j += p;			
		}
		
		do { p++; } while (!getBit(sieve, p));
	}
	
	printBigEndian(n);
	
	for (uint32_t i = 2; i <= n; i++) {
		if (getBit(sieve, i)){
			printBigEndian(i);
		}
	}
	
	return;
}

BIGNUM * initBn(void) {
    BIGNUM *bn_a = NULL;
    bn_a = BN_new();
	if (bn_a == NULL) {
		printError("Error allocating BigNum\n");
	}
    return bn_a;
}

//Test the primality of n using trial division 
TrialResult trialDivision(BIGNUM *n, uint32_t primes[], int nPrimes, uint32_t maxVal){
	BN_CTX *bn_ctx = NULL;
	bn_ctx = BN_CTX_new();
	if (bn_ctx == NULL) {
		printError("Error initializing BN CTX\n");
	}
	BN_CTX_init(bn_ctx);
	
	BIGNUM *bn_a = NULL;
	BIGNUM *bn_a_squared = NULL;
	BIGNUM *bn_mod = NULL;
	bn_a = initBn();
	bn_a_squared = initBn();
	bn_mod = initBn();
	
	TrialResult result = {PASS, 0};
	int currentP = 0;
	
	do {
		if (currentP >= nPrimes) {
			uint32_t temp = htonl(maxVal);
			if (BN_bin2bn((unsigned char *)(&temp), sizeof(uint32_t), bn_a) == NULL){
				printError("Error converting word to BIGNUM\n");
			}
			BN_sqr(bn_a_squared, bn_a, bn_ctx);
			if (BN_cmp(bn_a_squared, n) == -1){
				result.r = NOT_ENOUGH;
				break;
			}
			else {
				printError("Error reading more prime.");
			}
		}
		
		uint32_t a = primes[currentP++];
		uint32_t temp = htonl(a);
		if (BN_bin2bn((unsigned char *)(&temp), sizeof(uint32_t), bn_a) == NULL){
			printError("Error converting word to BIGNUM\n");
		}
		BN_sqr(bn_a_squared, bn_a, bn_ctx);
		
		if (BN_cmp(bn_a_squared, n) == 1){
			break;
		}
		
		BN_mod(bn_mod, n, bn_a, bn_ctx);
		if (BN_is_zero(bn_mod)){
			result.r = COMPOSITE;
			result.witness = a;			
			break;
		}
	} while(result.r == PASS);
	
	BN_CTX_free(bn_ctx);
	BN_free(bn_a);
	BN_free(bn_a_squared);
	BN_free(bn_mod);
	return result;
}

void factorN_1(BIGNUM *r, int *s, BIGNUM *a){
	*s = 1;
	BIGNUM *temp = initBn();
	BN_copy(temp, a);
	BN_rshift1(r, temp);
	while (!BN_is_odd(r)) {
		BN_copy(temp, r);
		BN_rshift1(r, temp);
		(*s)++;
	}
}

void word2Bn(uint32_t p, BIGNUM *a) {
    if (p == 0) {
        printError("Error reading more prime.");
    }
    
    uint32_t temp = htonl(p);
    if (BN_bin2bn((unsigned char *)(&temp), sizeof(uint32_t), a) == NULL){
        printError("Error converting word to BIGNUM\n");
    }
}

TrialResult millerRabinIndent(BIGNUM *n, uint32_t primes[], int nPrimes, uint32_t maxVal, int maxIter, bool indent){
	if (!indent) printf("n = %s\n", BN_bn2dec(n));
	
	BIGNUM *n_1 = initBn();
	const BIGNUM *one = BN_value_one();
	BN_sub(n_1, n, one);
	if (indent) printf("  ");
	printf("  n-1 = %s\n", BN_bn2dec(n_1));
	
	BIGNUM *r = initBn();
	int s = 0;
	factorN_1(r, &s, n_1);//1
	if (indent) printf("  ");
	printf("  s = %d\n", s);
	if (indent) printf("  ");
	printf("  r = %s\n", BN_bn2dec(r));
	
	BIGNUM *a = initBn();
	BIGNUM *y = initBn();
	BIGNUM *y2 = initBn();
	
	BN_CTX *bn_ctx = NULL;
	bn_ctx = BN_CTX_new();
	if (bn_ctx == NULL) {
		printError("Error initializing BN CTX\n");
	}
	BN_CTX_init(bn_ctx);
	
	TrialResult result = {PASS, 0};
	
	for (int i = 1, currentI = 0; i <= maxIter; i++) {			//2
		if (currentI >= nPrimes) {
			printError("Error reading more prime.");
		}
		
		uint32_t currentP = primes[currentI++];
		word2Bn(currentP, a);							//2.1
		
		if (BN_cmp(a, n_1) == 1){
			result.r = FAILURE;								//2.1.1
			break;
		}
		
		BN_mod_exp(y, a, r, n, bn_ctx);					//2.2
		int cmp = BN_cmp(y, n_1);
		if (indent) printf("  ");
		printf("  Itr %d of %d, a = %s, y = %s%s\n", i, maxIter, BN_bn2dec(a), BN_bn2dec(y),
			   cmp == 0 ? " (which is n-1)" : "");
		if (!BN_is_one(y) && (cmp != 0)){	//2.3
			int subIterations = s - 1;
			for (int j = 1; (j <= subIterations) && (cmp != 0); j++) {			//2.3.1
				BN_mod_sqr(y2, y, n, bn_ctx);					//2.3.1.1
				cmp = BN_cmp(y2, n_1);
				if (indent) printf("  ");
				printf("    j = %d of %d, y = %s%s\n", j, subIterations, BN_bn2dec(y2),
					   cmp == 0 ? " (which is n-1)" : "");
				if (BN_is_one(y2)){								//2.3.1.2
					result.r = COMPOSITE;
					break;
				}
				BN_copy(y, y2);				
			}
			if (result.r == COMPOSITE){
				result.witness = currentP;				
				break;
			}
			if (BN_cmp(y, n_1) != 0 ){						//2.3.2
				result.r = COMPOSITE;
				result.witness = currentP;
				break;
			}
		}
	}
	
	BN_CTX_free(bn_ctx);
	BN_free(y);
	BN_free(y2);
	BN_free(a);
	BN_free(r);
	BN_free(n_1);
	return result;
}

//Test the primality of n using Miller-Rabin with security parameter maxIter
TrialResult millerRabin(BIGNUM *n, uint32_t primes[], int nPrimes, uint32_t maxVal, int maxIter){
	return millerRabinIndent(n, primes, nPrimes, maxVal, maxIter, false);
}

void randomOddNum(int k, FILE *file, BIGNUM *bn){
	int x = k % 8 == 0 ? k / 8 : k/8 + 1;
	unsigned char buffer[x];
	
//	printf("k = %d \n", k);
//	printf("x = %d \n", x);
	
	size_t numberReadBytes = fread(buffer, sizeof(char), x, file);
	if (numberReadBytes == x){
		if (BN_bin2bn(buffer, x, bn) == NULL){
			printError("Error converting binary to BIGNUM\n");
		}
		
		BN_set_bit(bn, 0);
		BN_set_bit(bn, k -1);
		for (int i = k; i < (x * 8); i++) {
			BN_clear_bit(bn, i);
		}
		
//		printf("bn = %s \n", BN_bn2dec(bn));
	}
	else {
		fileError("Error reading random file\n");
	}
}

//Generate a k-bit probable prime using the Random-Search algorithm with security parameter maxIter. 
void randomSearch(BIGNUM *n, int k, uint32_t primes[], int nPrimes, uint32_t maxVal, int maxIter, FILE *random){
	bool found = false;
	for (int i = 1; !found; i++) {
		printf("RANDOM-SEARCH: iteration %d\n", i);
		randomOddNum(k, random, n);
		printf("  n = %s\n", BN_bn2dec(n));
		
		TrialResult r = trialDivision(n, primes, nPrimes, maxVal);
		if (r.r == COMPOSITE){
			printf("  n is composite by trial division (mod %d = 0)\n", r.witness);
			continue;
		}
		else {
			printf("  n passes trial division test\n");
		}
		
		if (!BN_is_odd(n)) {
			fprintf(stderr, "n is even. Is it 2? \n");
			continue;
		}
		
		r = millerRabinIndent(n, primes, nPrimes, maxVal, maxIter, true);
		if (r.r == COMPOSITE){
			printf("  Miller-Rabin found a strong witness %d\n", r.witness);
		}
		else if (r.r == PASS){
			printf("  Miller-Rabin declares n to be a prime number\n");
			found = true;
			return;
		}
	}
}

double getR(int m, int k, FILE *random) {
    if (k <= 2*m) {//4.1
		printf("  step 4, r = 50%%\n");
		return 0.5;
	}	
	else {//4.2
		unsigned char randomByte[1];
		while (true) {
			//4.2.1
			size_t numberReadBytes = fread(randomByte, sizeof(char), 1, random);
			if (numberReadBytes != 1) {
				fileError("Error reading random file\n");
			}
			
			double r = *randomByte / 255.0;
			//4.2.2
			r = 0.5 + r / 2.0;
			//4.2.3
			if ( k * (1 - r) > m){
				int percent = (int)(r * 100 + 0.5);
				printf("  step 4: random byte = %d, r = %d%%\n", *randomByte, percent);
				return r;
			}
		}
	}
}

//Generate a k-bit provable prime using the Maurer algorithm
void maurer(BIGNUM *n, int k, uint32_t primes[], int nPrimes, uint32_t maxVal, FILE *random, int level){
	printf("Maurer: level %d, k=%d\n", level, k);
	//1
	if (k <= 20){
		while (true) {
			//1.1
			randomOddNum(k, random, n);
			printf("  step 1.1, n = %s\n", BN_bn2dec(n));
			//1.2
			TrialResult r = trialDivision(n, primes, nPrimes, maxVal);
			if (r.r != COMPOSITE) {
				printf("    n passes trial division test\n");
				return;
			}
			else {
				printf("    n is composite by trial division (mod %d = 0)\n", r.witness);
			}
		}
	}
	
	//2
	int m = 20;
	
	//4
	double r = getR(m, k, random);
	int k2 = (int)(r * k) + 1;
	
	BN_CTX *bn_ctx = NULL;
	bn_ctx = BN_CTX_new();
	if (bn_ctx == NULL) {
		printError("Error initializing BN CTX\n");
	}
	BN_CTX_init(bn_ctx);
	
	//5
	BIGNUM *q = initBn();
	maurer(q, k2, primes, nPrimes, maxVal, random, level + 1);
	printf("Maurer: back to level %d, k=%d, q=%s\n", level, k, BN_bn2dec(q));
	
	//6
	int qNumBits = BN_num_bits(q);
	BIGNUM *twoK_2 = initBn();
	BN_lshift(twoK_2, BN_value_one(), k - 2);
	BIGNUM *bnI = initBn();
	BN_div(bnI, NULL, twoK_2, q, bn_ctx);
	
	//7
	BIGNUM *randomR = initBn();
	BIGNUM *rModI = initBn();
	BIGNUM *rQ = initBn();
	BIGNUM *twoR = initBn();
	BIGNUM *bn_a = initBn();
	BIGNUM *bn_b = initBn();
	BIGNUM *n_1 = initBn();
	BIGNUM *b_1 = initBn();
	BIGNUM *gcd = initBn();
	
	int itr = 0;
	while (true) {
		itr++;
		//7.1
		randomOddNum(k + 1 - qNumBits, random, randomR);
		BN_mod(rModI, randomR, bnI, bn_ctx);
		BN_add(randomR, rModI, bnI);
		BN_add(randomR, randomR, BN_value_one());
		
		BN_lshift1(twoR, randomR);		
		BN_mul(rQ, twoR, q, bn_ctx);
		BN_add(n, rQ, BN_value_one());
		
		printf("  step 7, itr %d: R = %s, n = %s\n", itr, BN_bn2dec(randomR), BN_bn2dec(n));
		//7.2
		TrialResult r = trialDivision(n, primes, nPrimes, maxVal);
		if (r.r != COMPOSITE){
			printf("    n passes trial division test\n");
			int nNumBits = BN_num_bits(n);
			//7.2.1
			bool found = false;
			do {
				randomOddNum(nNumBits, random, bn_a);
				BN_sub(n_1, n, BN_value_one());
				found = (BN_cmp(bn_a, BN_value_one()) == 1) && (BN_cmp(bn_a, n_1) == -1);
			} while (!found);
			printf("  step 7.2.1, itr %d: a = %s\n", itr, BN_bn2dec(bn_a));
			
			//7.2.2
			BN_mod_exp(bn_b, bn_a, n_1, n, bn_ctx);
			if (BN_is_one(bn_b)){
				BN_mod_exp(bn_b, bn_a, twoR, n, bn_ctx);
				BN_sub(b_1, bn_b, BN_value_one());
				BN_gcd(gcd, b_1, n, bn_ctx);
				if (BN_is_one(gcd)){
					break;
				}
			}
		}
		else {
			printf("    n is composite by trial division (mod %d = 0)\n", r.witness);
		}
	}
	
	BN_CTX_free(bn_ctx);
	BN_free(gcd);
	BN_free(b_1);
	BN_free(n_1);
	BN_free(bn_b);
	BN_free(bn_a);
	BN_free(twoR);
	BN_free(rQ);
	BN_free(rModI);
	BN_free(randomR);
	
	BN_free(bnI);
	BN_free(twoK_2);
	BN_free(q);
}

