//
//  Prime.h
//  HW7
//
//  Created by Cuong Dong on 4/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW7_Prime_h
#define HW7_Prime_h

#include <openssl/bn.h>
#include <stdint.h>

//Print all primes not greater than n to STDOUT in prime file format
// using sieve of Eratosthenes algorithm
void sieveOfEratosthenes(uint32_t n);

//Status of primality test algorithms
typedef enum {PASS, NOT_ENOUGH, COMPOSITE, FAILURE} Result;

//Result of primality test algorithms: witness is non-zero if r is COMPOSITE
typedef struct {
	Result r;
	uint32_t witness;
} TrialResult;

//Test the primality of n using trial division 
TrialResult trialDivision(BIGNUM *n, uint32_t primes[], int nPrimes, uint32_t maxVal);

//Test the primality of n using Miller-Rabin with security parameter maxIter
TrialResult millerRabin(BIGNUM *n, uint32_t primes[], int nPrimes, uint32_t maxVal, int maxIter);

//Read prime file into buffer
uint32_t readPrimeFile(uint32_t primes[], long count, FILE *primeFile);

//Generate a k-bit probable prime using the Random-Search algorithm with security parameter maxIter. 
void randomSearch(BIGNUM *n, int k, uint32_t primes[], int nPrimes, uint32_t maxVal, int maxIter, FILE *random);

//Generate a k-bit provable prime using the Maurer algorithm
void maurer(BIGNUM *n, int k, uint32_t primes[], int nPrimes, uint32_t maxVal, FILE *random, int level);
#endif
