//
//  Util.h
//  HW7
//
//  Created by Cuong Dong on 4/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW7_Util_h
#define HW7_Util_h

#include <stdbool.h>

//Print the provided message to stderr and exit
void printError(const char *message);

void fileError(const char *message);

//Open a file with provided name as binary for writing
FILE* openOutputFile(const char * name);

//Open a file with provided name as binary for reading
FILE * openFile(const char *fileName);

//Get/Set n-th bit in buffer
void setBit(char buf[], int n, bool set);
bool getBit(char buf[], int n);

long fileSize(FILE * file);

#endif
