//
//  CommandParser.h
//  HW7
//
//  Created by Cuong Dong on 4/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#ifndef HW7_CommandParser_h
#define HW7_CommandParser_h

#include <stdbool.h>
#include <stdint.h>

//Enumeration of all possible commands
typedef enum {PRIMES, TRIAL_DIV, MILLER_RABIN, RND_SEARCH, MAURER} Command;

//struct to capture all possible command options
typedef struct {
    Command command;
	uint32_t n; //for primes only
	
	int maxItr;
	int k;
	const char *bigN;
    const char *primeFile;
	const char *randomFile;	
} CommandOptions;

//Parse the command line argument and return CommandOptions struct
CommandOptions parseCommandLine(int argc, char *argv[]);

#endif
