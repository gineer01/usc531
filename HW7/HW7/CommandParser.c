//
//  CommandParser.c
//  HW7
//
//  Created by Cuong Dong on 4/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "CommandParser.h"


#define NUMBER "n"
#define PRIME_FILE "p"
#define MAX_ITER "t"
#define NUM_BITS "k"
#define RANDOM_FILE "r"


//Functions to parse command options after the command is known
CommandOptions parsePrime(int argc, char *argv[]);
CommandOptions parseTrialDiv(int argc, char *argv[]);
CommandOptions parseMillerRabin(int argc, char *argv[]);
CommandOptions parseRandomSearch(int argc, char *argv[]);
CommandOptions parseMaurer(int argc, char *argv[]);

//Print usage to user when an invalid command option is found and exit
void printUsage(){
    char * usage = "The commandline syntax for hw7 is as follows:\n"
    "hw7 primes -n=maxval\n"
    "hw7 trialdiv -n=number -p=primesfile\n"
    "hw7 millerrabin -n=number -t=maxitr -p=primesfile\n"
    "hw7 rndsearch -k=numbits -t=maxitr -p=primesfile -r=rndfile\n"
    "hw7 maurer -k=numbits -p=primesfile -r=rndfile\n";
    fprintf(stderr, "%s", usage);
    exit(1);
}

//Print an error message and then print the proper usage and exit
void printErrorAndUsage(const char *message) {
    fprintf(stderr, "%s", message);
    printUsage();
}

//A pair of key and value (e.g., p="password")
typedef struct {
    const char *option;
    const char *value;
} Pair;

//Parse a string of format <key>=<value> to a Pair
Pair getPair(char *argv){
    Pair pair = {"", ""};
    char *option = argv;
    for(char *value = argv; *value != 0; value++){
        if (*value == '='){
            *value = 0;
            value++;
            pair.option = option;
            pair.value = value;
            return pair;
        }
    }
    
    printErrorAndUsage("Wrong command option (missing =)\n");
    
    return pair;
}

//Parse the command and dispatch to the corresponding function
CommandOptions parseCommandLine(int argc, char *argv[]){
    if (argc <= 1) {
        printErrorAndUsage("Please provide a command\n");
    }
    
    if (strcmp(argv[1], "primes") == 0) {
        return parsePrime(argc, argv);
    } else if (strcmp(argv[1], "trialdiv") == 0) {
        return parseTrialDiv(argc, argv);
    } else if (strcmp(argv[1], "millerrabin") == 0) {
        return parseMillerRabin(argc, argv);
    } else if (strcmp(argv[1], "rndsearch") == 0) {
        return parseRandomSearch(argc, argv);
    } else if (strcmp(argv[1], "maurer") == 0) {
        return parseMaurer(argc, argv);
    }else {
        printErrorAndUsage("Invalid command \n");
    }
    
    exit(1);
}

CommandOptions parsePrime(int argc, char *argv[]){
	CommandOptions options = {PRIMES, 0, 0, 0, "", "", ""};
    if (argc == 3){
        for (int i = 2; i < 3; i++) {
            if (argv[i][0] == '-'){
                Pair pair = getPair(argv[i] + 1);
                if (strcmp(pair.option, NUMBER) == 0){
                    options.n = atoi(pair.value);
                    if (options.n < 1){
                        printErrorAndUsage("Max value must be a positive integer.\n");
                    }
					else if (options.n > 1<<24){
						printErrorAndUsage("Max value is out of range 2^24.\n");
					}
                }
                else {
                    printErrorAndUsage("Invalid command option.\n");
                }
            }
            else {
                printErrorAndUsage("Invalid command option (missing -) \n");
            }
        }
    }
    else {
        printErrorAndUsage("Invalid number of arguments \n");
    }
    
    return options;
}

CommandOptions parseTrialDiv(int argc, char *argv[]){
	CommandOptions options = {TRIAL_DIV, 0, 0, 0, "", "", ""};
	if (argc != 4){
        printErrorAndUsage("Invalid number of arguments.\n");
    }
	
	for (int i = 2; i < 4; i++) {
        if (argv[i][0] == '-'){
            Pair pair = getPair(argv[i] + 1);
            if (strcmp(pair.option, NUMBER) == 0){
                options.bigN = pair.value;
            }
            else if (strcmp(pair.option, PRIME_FILE) == 0){
                options.primeFile = pair.value;
            }
            else {
                printErrorAndUsage("Invalid command option.\n");
            }
        }
        else {
            printErrorAndUsage("Invalid command option (missing -) \n");
        }
    }
	
	return options;
}

CommandOptions parseMillerRabin(int argc, char *argv[]){
	CommandOptions options = {MILLER_RABIN, 0, 0, 0, "", "", ""};
	if (argc != 5){
        printErrorAndUsage("Invalid number of arguments.\n");
    }
	
	for (int i = 2; i < argc; i++) {
        if (argv[i][0] == '-'){
            Pair pair = getPair(argv[i] + 1);
            if (strcmp(pair.option, NUMBER) == 0){
                options.bigN = pair.value;
            }
            else if (strcmp(pair.option, PRIME_FILE) == 0){
                options.primeFile = pair.value;
            }
            else if (strcmp(pair.option, MAX_ITER) == 0){
				options.maxItr = atoi(pair.value);
				if (options.maxItr < 1){
					printErrorAndUsage("Max iteration value must be a positive integer.\n");
				}
			}
			else {
                printErrorAndUsage("Invalid command option.\n");
            }
        }
        else {
            printErrorAndUsage("Invalid command option (missing -) \n");
        }
    }
	return options;
}

CommandOptions parseRandomSearch(int argc, char *argv[]){
	CommandOptions options = {RND_SEARCH, 0, 0, 0, "", "", ""};
	
	if (argc != 6){
        printErrorAndUsage("Invalid number of arguments.\n");
    }
	
	for (int i = 2; i < argc; i++) {		
		if (argv[i][0] == '-'){
            Pair pair = getPair(argv[i] + 1);
            if (strcmp(pair.option, NUM_BITS) == 0){
				options.k = atoi(pair.value);
				if (options.k < 1){
					printErrorAndUsage("Number-of-bits value must be a positive integer.\n");
				}
            }
            else if (strcmp(pair.option, PRIME_FILE) == 0){
                options.primeFile = pair.value;
            }
            else if (strcmp(pair.option, RANDOM_FILE) == 0){
                options.randomFile = pair.value;
            }
            else if (strcmp(pair.option, MAX_ITER) == 0){
				options.maxItr = atoi(pair.value);
				if (options.maxItr < 1){
					printErrorAndUsage("Max iteration value must be a positive integer.\n");
				}
			}
			else {
                printErrorAndUsage("Invalid command option.\n");
            }
        }
        else {
            printErrorAndUsage("Invalid command option (missing -) \n");
        }
    }
	
	return options;
}

CommandOptions parseMaurer(int argc, char *argv[]){
	CommandOptions options = {MAURER, 0, 0, 0, "", "", ""};
	
	if (argc != 5){
        printErrorAndUsage("Invalid number of arguments.\n");
    }
	
	for (int i = 2; i < argc; i++) {
		if (argv[i][0] == '-'){
            Pair pair = getPair(argv[i] + 1);
            if (strcmp(pair.option, NUM_BITS) == 0){
				options.k = atoi(pair.value);
				if (options.k < 1){
					printErrorAndUsage("Number-of-bits value must be a positive integer.\n");
				}
            }
            else if (strcmp(pair.option, PRIME_FILE) == 0){
                options.primeFile = pair.value;
            }
            else if (strcmp(pair.option, RANDOM_FILE) == 0){
                options.randomFile = pair.value;
            }            
			else {
                printErrorAndUsage("Invalid command option.\n");
            }
        }
        else {
            printErrorAndUsage("Invalid command option (missing -) \n");
        }
    }

	
	return options;
}

