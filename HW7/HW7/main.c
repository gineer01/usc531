//
//  main.c
//  HW7
//
//  Created by Cuong Dong on 4/28/13.
//  Copyright (c) 2013 Cuong Dong. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <openssl/bn.h>
#include "CommandParser.h"
#include "Util.h"
#include "Prime.h"

//Functions to execute the selected command with the provided options
int doPrime(CommandOptions options);
int doTrialDiv(CommandOptions options);
int doMillerRabin(CommandOptions options);
int doRandomSearch(CommandOptions options);
int doMaurer(CommandOptions options);


//Parse the command line arguments and dispatch to the corresponding function
int main(int argc, char * argv[]){
    CommandOptions options = parseCommandLine(argc, argv);
    switch(options.command){
        case PRIMES: return doPrime(options);
        case TRIAL_DIV: return doTrialDiv(options);
        case MILLER_RABIN: return doMillerRabin(options);
        case RND_SEARCH: return doRandomSearch(options);
		case MAURER: return doMaurer(options);
        default:
            return 0;
    }
}

int doPrime(CommandOptions options){
	sieveOfEratosthenes(options.n);
	
	return 0;
}

int doTrialDiv(CommandOptions options){
	FILE *primeFile = openFile(options.primeFile);
	
	BIGNUM *bn_a = NULL;
	int result = BN_dec2bn(&bn_a, options.bigN);
	if (result == 0){
		printError("Error converting string to BIGNUM");
	}
	
	int count = fileSize(primeFile)/4 - 1;
	uint32_t primes[count];
	uint32_t maxVal = readPrimeFile(primes, count, primeFile);
	
	TrialResult r = trialDivision(bn_a, primes, count, maxVal);
	
	switch (r.r) {
		case NOT_ENOUGH:
			printf("n passes trial division test (not enough primes)\n");
			break;
		case PASS:
			printf("n passes trial division test\n");
			break;
		case COMPOSITE:
			printf("n is composite by trial division (mod %d = 0)\n", r.witness);
			break;
		default:
			break;
	}
	
	fclose(primeFile);
	BN_free(bn_a);
	return 0;
}

int doMillerRabin(CommandOptions options){
	FILE *primeFile = openFile(options.primeFile);
	
	BIGNUM *bn_a = NULL;
	int result = BN_dec2bn(&bn_a, options.bigN);
	if (result == 0){
		printError("Error converting string to BIGNUM");
	}
	
	int count = fileSize(primeFile)/4 - 1;
	uint32_t primes[count];
	uint32_t maxVal = readPrimeFile(primes, count, primeFile);
	
	TrialResult r = millerRabin(bn_a, primes, count, maxVal, options.maxItr);
	
	switch (r.r) {
		case PASS:
			printf("Miller-Rabin declares n to be a prime number\n");
			break;
		case FAILURE:
			fprintf(stderr, "Failure returned by Miller-Rabin\n");
			break;
		case COMPOSITE:
			printf("Miller-Rabin found a strong witness %d\n", r.witness);
		default:
			break;
	}
	
	fclose(primeFile);
	BN_free(bn_a);
	return 0;
}

int doRandomSearch(CommandOptions options){
	FILE *primeFile = openFile(options.primeFile);
	FILE *randomFile = openFile(options.randomFile);
	
	BIGNUM *bn_a = NULL;
    bn_a = BN_new();
	if (bn_a == NULL) {
		printError("Error allocating BigNum\n");
	}
	
	int count = fileSize(primeFile)/4 - 1;
	uint32_t primes[count];
	uint32_t maxVal = readPrimeFile(primes, count, primeFile);
	
	randomSearch(bn_a, options.k, primes, count, maxVal, options.maxItr, randomFile);
	
	fclose(primeFile);
	fclose(randomFile);
	BN_free(bn_a);
	return 0;
}

int doMaurer(CommandOptions options){
	FILE *primeFile = openFile(options.primeFile);
	FILE *randomFile = openFile(options.randomFile);
	
	BIGNUM *bn_a = NULL;
    bn_a = BN_new();
	if (bn_a == NULL) {
		printError("Error allocating BigNum\n");
	}
	
	int count = fileSize(primeFile)/4 - 1;
	uint32_t primes[count];
	uint32_t maxVal = readPrimeFile(primes, count, primeFile);
	
	maurer(bn_a, options.k, primes, count, maxVal, randomFile, 0);
	
	printf("\nMaurer's Algorithm found an %d-bit prime:\n", options.k);
	printf("  n = %s\n", BN_bn2dec(bn_a));
	
	fclose(primeFile);
	fclose(randomFile);
	BN_free(bn_a);
	return 0;
}

